<style type="text/css">

    .text-secondary-d1 {
        color: #728299!important;
    }
    .page-header {
        margin: 0 0 1rem!important;
        padding-bottom: 1rem!important;
        padding-top: .5rem!important;
        border-bottom: 1px dotted #e2e2e2!important;
        display: -ms-flexbox!important;
        display: flex!important;
        -ms-flex-pack: justify!important;
        justify-content: space-between!important;
        -ms-flex-align: center!important;
        align-items: center!important;
    }
    .page-title {
        padding: 0!important;
        margin: 0!important;
        font-size: 1.75rem;
        font-weight: 300!important;
    }
    .brc-default-l1 {
        border-color: #dce9f0!important;
    }

    .ml-n1, .mx-n1 {
        margin-left: -.25rem!important;
    }
    .mr-n1, .mx-n1 {
        margin-right: -.25rem!important;
    }
    .mb-4, .my-4 {
        margin-bottom: 1.5rem!important;
    }

    hr {
        margin-top: 1rem!important;
        margin-bottom: 1rem!important;
        border: 0!important;
        border-top: 1px solid rgba(0,0,0,.1)!important;
    }

    .text-grey-m2 {
        color: #888a8d!important;
    }

    .text-success-m2 {
        color: #86bd68!important;
    }

    .font-bolder, .text-600 {
        font-weight: 600!important;
    }

    .text-110 {
        font-size: 110%!important;
    }
    .text-blue {
        color: #478fcc!important;
    }
    .pb-25, .py-25 {
        padding-bottom: .75rem!important;
    }

    .pt-25, .py-25 {
        padding-top: .75rem!important;
    }
    .bgc-default-tp1 {
        background-color: rgba(121,169,197,.92)!important;
    }
    .bgc-default-l4, .bgc-h-default-l4:hover {
        background-color: #f3f8fa!important;
    }
    .page-header .page-tools {
        align-self: flex-end;
    }

    .btn-light {
        color: #757984!important;
        background-color: #f5f6f9!important;
        border-color: #dddfe4!important;
    }
    .w-2 {
        width: 1rem!important;
    }

    .text-120 {
        font-size: 120%!important;
    }
    .text-primary-m1 {
        color: #4087d4!important;
    }

    .text-danger-m1 {
        color: #dd4949!important;
    }
    .text-blue-m2 {
        color: #68a3d5!important;
    }
    .text-150 {
        font-size: 150%!important;
    }
    .text-60 {
        font-size: 60%!important;
    }
    .text-grey-m1 {
        color: #7b7d81!important;
    }
    .align-bottom {
        vertical-align: bottom!important;
    }
</style>

<?php if( $meta_count == '0' ) { ?>
    <div class="modal-content animated flipInY">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
            <h5 class="modal-title">View Vehicle Detail</h5>
        </div>
        <div class="page-content container">
            <div class="container px-0">
                <div class="row mt-4">
                    <div class="col-12 col-lg-6 offset-lg-1">
                        <hr class="row brc-default-l1 mx-n1 mb-4" />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-grey-m2">
                                    <div class="my-1" style="height: 30px;">
                                        Vehicle: <span class="label label-success" style="float: right;">   Not Registered </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="mt-4">
                            <div class="row text-600 text-white bgc-default-tp1 py-25">
                                <div class="col-1 col-sm-2">#</div>
                                <div class="col-9 col-sm-5">Key</div>
                                <div class="col-9 col-sm-5">Value</div>
                            </div>
                            <div class="text-95 text-secondary-d3">
                                <div class="row mb-2 mb-sm-0 py-25">
                                    <div class="col-1 col-sm-2">1</div>
                                    <div class="col-9 col-sm-5">Empty</div>
                                    <div class="col-9 col-sm-5">Null</div>
                                </div>
                            </div>
                            <div class="row border-b-2 brc-default-l2"></div>
                            <hr />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="modal-content animated flipInY">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
            <h5 class="modal-title">View Vehicle Detail</h5>
        </div>
        <div class="page-content container">
            <div class="container px-0">
                <div class="row mt-4">
                    <div class="col-12 col-lg-6 offset-lg-1">
                        <hr class="row brc-default-l1 mx-n1 mb-4" />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-grey-m2">
                                    <div class="my-1" style="height: 30px;">
                                        Vehicle owner Name: <span class="label label-success" style="float: right;">   <?php echo get_user_name( $vehicles['user_id'] ); ?> </span>
                                    </div>
                                    <div class="my-1" style="height: 30px;">
                                        Vehicle company Name:  <span class="label label-warning" style="float: right;"> <?php echo $vehicles['company_name']; ?> </span>
                                    </div>
                                    <div class="my-1" style="height: 30px;">
                                        Vehicle registration Number:  <span class="label label-primary" style="float: right;">  <?php echo $vehicles['reg_no']; ?> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4">
                            <div class="row text-600 text-white bgc-default-tp1 py-25">
                                <div class="col-1 col-sm-2">#</div>
                                <div class="col-9 col-sm-5">Key</div>
                                <div class="col-9 col-sm-5">Value</div>
                            </div>
                            <div class="text-95 text-secondary-d3">
                                <?php if( $meta_count > 1 ) { foreach ($vehicle_meta as $key => $value) { ?>
                                    <div class="row mb-2 mb-sm-0 py-25">
                                        <div class="col-1 col-sm-2"><?php echo $key;?></div>
                                        <div class="col-9 col-sm-5"><?php echo $value['meta_key'];?></div>
                                        <div class="col-9 col-sm-5"><?php echo $value['meta_value'];?></div>
                                    </div>
                                <?php } } else {?>
                                    <div class="row mb-2 mb-sm-0 py-25">
                                        <div class="col-1 col-sm-2">1</div>
                                        <div class="col-9 col-sm-5">Empty</div>
                                        <div class="col-9 col-sm-5">Null</div>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="row border-b-2 brc-default-l2"></div>
                            <hr />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


