<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Welcome to CodeIgniter</title>
      <?php $this->load->view('common/header'); ?> 
      <style type="text/css">
          



         /****** Searchable container */

         .title{
          margin-left:20px
         }

         .fa-user{
          font-size:80px   
         }

         .searchable-container{
             margin-top:40px;
         }

         .glyphicon-lg{
             font-size:4em
         }
         .info-block{
             border-right:5px solid #E6E6E6;margin-bottom:25px
         }
         .info-block .square-box {
             width:120px;
             min-height:120px;
             margin-right:22px;
             text-align:center!important;
             background-color:#676767;
             padding:20px 0
         }
         .info-block:hover .info-block.block-info {
             border-color:#20819e
         }

         .info-block.block-info .square-box {
             background-color:#5bc0de;
             color:#FFF
         }






         /*   */

         body{margin-top:20px;
         background:#eee;
         }

         .btn-compose-email {
             padding: 10px 0px;
             margin-bottom: 20px;
         }

         .btn-danger {
             background-color: #E9573F;
             border-color: #E9573F;
             color: white;
         }

         .panel-teal .panel-heading {
             background-color: #37BC9B;
             border: 1px solid #36b898;
             color: white;
         }

         .panel .panel-heading {
             padding: 5px;
             border-top-right-radius: 3px;
             border-top-left-radius: 3px;
             border-bottom: 1px solid #DDD;
             -moz-border-radius: 0px;
             -webkit-border-radius: 0px;
             border-radius: 0px;
         }

         .panel .panel-heading .panel-title {
             padding: 10px;
             font-size: 17px;
         }

         form .form-group {
             position: relative;
             margin-left: 0px !important;
             margin-right: 0px !important;
         }

         .inner-all {
             padding: 10px;
         }

         /* ========================================================================
          * MAIL
          * ======================================================================== */
         .nav-email > li:first-child + li:active {
           margin-top: 0px;
         }
         .nav-email > li + li {
           margin-top: 1px;
         }
         .nav-email li {
           background-color: white;
         }
         .nav-email li.active {
           background-color: transparent;
         }
         .nav-email li.active .label {
           background-color: white;
           color: black;
         }
         .nav-email li a {
           color: black;
           -moz-border-radius: 0px;
           -webkit-border-radius: 0px;
           border-radius: 0px;
         }
         .nav-email li a:hover {
           background-color: #EEEEEE;
         }
         .nav-email li a i {
           margin-right: 5px;
         }
         .nav-email li a .label {
           margin-top: -1px;
         }

         .table-email tr:first-child td {
           border-top: none;
         }
         .table-email tr td {
           vertical-align: top !important;
         }
         .table-email tr td:first-child, .table-email tr td:nth-child(2) {
           text-align: center;
           width: 35px;
         }
         .table-email tr.unread, .table-email tr.selected {
           background-color: #EEEEEE;
         }
         .table-email .media {
           margin: 0px;
           padding: 0px;
           position: relative;
         }
         .table-email .media h4 {
           margin: 0px;
           font-size: 14px;
           line-height: normal;
         }
         .table-email .media-object {
           width: 35px;
           -moz-border-radius: 2px;
           -webkit-border-radius: 2px;
           border-radius: 2px;
         }
         .table-email .media-meta, .table-email .media-attach {
           font-size: 11px;
           color: #999;
           position: absolute;
           right: 10px;
         }
         .table-email .media-meta {
           top: 0px;
         }
         .table-email .media-attach {
           bottom: 0px;
         }
         .table-email .media-attach i {
           margin-right: 10px;
         }
         .table-email .media-attach i:last-child {
           margin-right: 0px;
         }
         .table-email .email-summary {
           margin: 0px 110px 0px 0px;
         }
         .table-email .email-summary strong {
           color: #333;
         }
         .table-email .email-summary span {
           line-height: 1;
         }
         .table-email .email-summary span.label {
           padding: 1px 5px 2px;
         }
         .table-email .ckbox {
           line-height: 0px;
           margin-left: 8px;
         }
         .table-email .star {
           margin-left: 6px;
         }
         .table-email .star.star-checked i {
           color: goldenrod;
         }

         .nav-email-subtitle {
           font-size: 15px;
           text-transform: uppercase;
           color: #333;
           margin-bottom: 15px;
           margin-top: 30px;
         }

         .compose-mail {
           position: relative;
           padding: 15px;
         }
         .compose-mail textarea {
           width: 100%;
           padding: 10px;
           border: 1px solid #DDD;
         }

         .view-mail {
           padding: 10px;
           font-weight: 300;
         }

         .attachment-mail {
           padding: 10px;
           width: 100%;
           display: inline-block;
           margin: 20px 0px;
           border-top: 1px solid #EFF2F7;
         }
         .attachment-mail p {
           margin-bottom: 0px;
         }
         .attachment-mail a {
           color: #32323A;
         }
         .attachment-mail ul {
           padding: 0px;
         }
         .attachment-mail ul li {
           float: left;
           width: 200px;
           margin-right: 15px;
           margin-top: 15px;
           list-style: none;
         }
         .attachment-mail ul li a.atch-thumb img {
           width: 200px;
           margin-bottom: 10px;
         }
         .attachment-mail ul li a.name span {
           float: right;
           color: #767676;
         }

         @media (max-width: 640px) {
           .compose-mail-wrapper .compose-mail {
             padding: 0px;
           }
         }
         @media (max-width: 360px) {
           .mail-wrapper .panel-sub-heading {
             text-align: center;
           }
           .mail-wrapper .panel-sub-heading .pull-left, .mail-wrapper .panel-sub-heading .pull-right {
             float: none !important;
             display: block;
           }
           .mail-wrapper .panel-sub-heading .pull-right {
             margin-top: 10px;
           }
           .mail-wrapper .panel-sub-heading img {
             display: block;
             margin-left: auto;
             margin-right: auto;
             margin-bottom: 10px;
           }
           .mail-wrapper .panel-footer {
             text-align: center;
           }
           .mail-wrapper .panel-footer .pull-right {
             float: none !important;
             margin-left: auto;
             margin-right: auto;
           }
           .mail-wrapper .attachment-mail ul {
             padding: 0px;
           }
           .mail-wrapper .attachment-mail ul li {
             width: 100%;
           }
           .mail-wrapper .attachment-mail ul li a.atch-thumb img {
             width: 100% !important;
           }
           .mail-wrapper .attachment-mail ul li .links {
             margin-bottom: 20px;
           }

           .compose-mail-wrapper .search-mail input {
             width: 130px;
           }
           .compose-mail-wrapper .panel-sub-heading {
             padding: 10px 7px;
           }
         }










         /*font Awesome http://fontawesome.io*/
         @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
         /*Comment List styles*/
         .comment-list .row {
           margin-bottom: 0px;
         }
         .comment-list .panel .panel-heading {
           padding: 4px 15px;
           position: absolute;
           border:none;
           /*Panel-heading border radius*/
           border-top-right-radius:0px;
           top: 1px;
         }
         .comment-list .panel .panel-heading.right {
           border-right-width: 0px;
           /*Panel-heading border radius*/
           border-top-left-radius:0px;
           right: 16px;
         }
         .comment-list .panel .panel-heading .panel-body {
           padding-top: 6px;
         }
         .comment-list figcaption {
           /*For wrapping text in thumbnail*/
           word-wrap: break-word;
         }
         /* Portrait tablets and medium desktops */
         @media (min-width: 768px) {
           .comment-list .arrow:after, .comment-list .arrow:before {
             content: "";
             position: absolute;
             width: 0;
             height: 0;
             border-style: solid;
             border-color: transparent;
           }
           .comment-list .panel.arrow.left:after, .comment-list .panel.arrow.left:before {
             border-left: 0;
           }
           /*****Left Arrow*****/
           /*Outline effect style*/
           .comment-list .panel.arrow.left:before {
             left: 0px;
             top: 30px;
             /*Use boarder color of panel*/
             border-right-color: inherit;
             border-width: 16px;
           }
           /*Background color effect*/
           .comment-list .panel.arrow.left:after {
             left: 1px;
             top: 31px;
             /*Change for different outline color*/
             border-right-color: #FFFFFF;
             border-width: 15px;
           }
           /*****Right Arrow*****/
           /*Outline effect style*/
           .comment-list .panel.arrow.right:before {
             right: -16px;
             top: 30px;
             /*Use boarder color of panel*/
             border-left-color: inherit;
             border-width: 16px;
           }
           /*Background color effect*/
           .comment-list .panel.arrow.right:after {
             right: -14px;
             top: 31px;
             /*Change for different outline color*/
             border-left-color: #FFFFFF;
             border-width: 15px;
           }
         }
         .comment-list .comment-post {
           margin-top: 6px;
         }







         /**** resumee ****/
                             
             /* uses font awesome for social icons */
         @import url(http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);

         .page-header{
           text-align: center;    
         }

         /*social buttons*/
         .btn-social{
           color: white;
           opacity:0.9;
         }
         .btn-social:hover {
           color: white;
             opacity:1;
         }
         .btn-facebook {
         background-color: #3b5998;
         opacity:0.9;
         }
         .btn-twitter {
         background-color: #00aced;
         opacity:0.9;
         }
         .btn-linkedin {
         background-color:#0e76a8;
         opacity:0.9;
         }
         .btn-github{
           background-color:#000000;
           opacity:0.9;
         }
         .btn-google {
           background-color: #c32f10;
           opacity: 0.9;
         }
         .btn-stackoverflow{
           background-color: #D38B28;
           opacity: 0.9;
         }

         /* resume stuff */

         .bs-callout {
             -moz-border-bottom-colors: none;
             -moz-border-left-colors: none;
             -moz-border-right-colors: none;
             -moz-border-top-colors: none;
             border-color: #eee;
             border-image: none;
             border-radius: 3px;
             border-style: solid;
             border-width: 1px 1px 1px 5px;
             margin-bottom: 5px;
             padding: 20px;
         }
         .bs-callout:last-child {
             margin-bottom: 0px;
         }
         .bs-callout h4 {
             margin-bottom: 10px;
             margin-top: 0;
         }

         .bs-callout-danger {
             border-left-color: #d9534f;
         }

         .bs-callout-danger h4{
             color: #d9534f;
         }

         .resume .list-group-item:first-child, .resume .list-group-item:last-child{
           border-radius:0;
         }

         /*makes an anchor inactive(not clickable)*/
         .inactive-link {
            pointer-events: none;
            cursor: default;
         }

         .resume-heading .social-btns{
           margin-top:15px;
         }
         .resume-heading .social-btns i.fa{
           margin-left:-5px;
         }



         @media (max-width: 992px) {
           .resume-heading .social-btn-holder{
             padding:5px;
           }
         }


         /* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */

         .progress-bar {
             text-align: left;
             white-space: nowrap;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            cursor: pointer;
         }

         .progress-bar > .progress-type {
            padding-left: 10px;
         }

         .progress-meter {
            min-height: 15px;
            border-bottom: 2px solid rgb(160, 160, 160);
           margin-bottom: 15px;
         }

         .progress-meter > .meter {
            position: relative;
            float: left;
            min-height: 15px;
            border-width: 0px;
            border-style: solid;
            border-color: rgb(160, 160, 160);
         }

         .progress-meter > .meter-left {
            border-left-width: 2px;
         }

         .progress-meter > .meter-right {
            float: right;
            border-right-width: 2px;
         }

         .progress-meter > .meter-right:last-child {
            border-left-width: 2px;
         }

         .progress-meter > .meter > .meter-text {
            position: absolute;
            display: inline-block;
            bottom: -20px;
            width: 100%;
            font-weight: 700;
            font-size: 0.85em;
            color: rgb(160, 160, 160);
            text-align: left;
         }

         .progress-meter > .meter.meter-right > .meter-text {
            text-align: right;
         }


             
                                 
         /**** resume ****/
       </style>
   </head>
   <body> 
      <div class="container" style="margin-top: 60px;">
         <div class="row">
             <div class="col-sm-3">
                 <!-- <a href="mail-compose.html" class="btn btn-danger btn-block btn-compose-email">Compose Email</a> -->
                 <ul class="nav nav-pills nav-stacked nav-email shadow mb-20">
                     <li class="active">
                         <a href="#profile">
                             <i class="fa fa-inbox"></i> Profile 
                         </a>
                     </li>
                     <!-- <li>
                         <a href="#requests"><i class="fa fa-envelope-o"></i> Send Request</a>
                     </li> -->
                     <!-- <li>
                         <a href="#personal_vehicles"><i class="fa fa-certificate"></i> Personal Vehicles</a>
                     </li> --> 
                 </ul> 

                 <h5 class="nav-email-subtitle">More</h5>
                 <ul class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow"> 
                     <li> 
                         <a href="<?php echo admin_url(); ?>logout/frontend">
                             <i class="fa fa-sign-out"></i> Logout
                         </a>
                     </li> 
             </div>
            <div class="col-sm-9" id="profile">  
               <div class="panel panel-default">
                  <div class="panel-heading resume-heading">
                     <div class="row">
                        <div class="col-lg-12"> 
                           <div class="col-xs-12 col-sm-12">
                              <ul class="list-group">
                                 <li class="list-group-item"><i class="fa fa-users"></i> <?php echo "    ".get_session('admin_username'); ?> </li>  
                                 <li class="list-group-item"><i class="fa fa-phone"></i><?php echo "    ".get_session('admin_phone'); ?> </li>
                                 <li class="list-group-item"><i class="fa fa-envelope"></i><?php echo "    ".get_session('admin_email'); ?></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="bs-callout bs-callout-danger">
                     <div class="row">
                        <div class="col-md-6" style="float: left"><h4>Personal Vehicles</h4></div>
                        <div class="col-md-6" style="float: right">
                           <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_vehicle_modal">
                             <i class='fa fa-plus'></i> Add Vehicle
                         </button>
                        </div>
                     </div> 
                     <hr>
                     <div class="row">
                         <div class="col-lg-12">
                             <div class="ibox"> 
                                 <div class="ibox-content">
                                     <div class="table-responsive">
                                         <table id="vehicles-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                             <thead>
                                             <tr>
                                                 <th>Sr#</th> 
                                                 <th>Owner Name</th>
                                                 <th>Brand</th>
                                                 <th>Model</th>
                                                 <th>Year of make</th>
                                                 <th>Registration Number</th>
                                                 <th>Date</th>
                                                 <th>Action</th>
                                             </tr>
                                             </thead>
                                             <tbody>
                                             <?php $i=1; foreach ($vehicles as $vehicle){ ?>
                                                 <tr id="<?php echo "row_".$i; ?>">
                                                     <td><?php echo $i; ?></td> 
                                                     <td><?php echo get_user_name( $vehicle['user_id'] ); ?></td>
                                                     <td><?php echo $vehicle['company_name']; ?></td>
                                                     <td><?php echo $vehicle['model']; ?></td>
                                                     <td><?php echo $vehicle['year_of_make']; ?></td>
                                                     <td><?php echo $vehicle['reg_no']; ?></td>
                                                     <td><?php echo date('F jS, Y - h:i a' ,strtotime($vehicle['created_at'])); ?></td>  
                                                     <td>
                                                         <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $vehicle['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button> 
                                                     </td>
                                                 </tr>
                                                 <?php $i++; } ?>
                                             </tbody>
                                         </table>
                                     </div>
                                 </div>
                              </div>
                           </div>
                        </div> 
                     </div>
                     <div class="bs-callout bs-callout-danger">
                     <div class="row">
                        <div class="col-md-6" style="float: left"><h4>Request Complain List</h4></div>
                        <div class="col-md-6" style="float: right">
                           <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_complain_modal">
                             <i class='fa fa-plus'></i> Add Complain
                         </button>
                        </div>
                     </div> 
                     <hr>
                     <div class="row">
                         <div class="col-lg-12">
                             <div class="ibox"> 
                                 <div class="ibox-content">
                                     <div class="table-responsive">
                                         <table id="complain-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                             <thead>
                                             <tr>
                                                 <th>Sr#</th> 
                                                 <th>Complain</th>
                                                 <th>Approved by Name</th> 
                                                 <th>Status</th> 
                                                 <th>Date</th> 
                                                 <th>Action</th> 
                                             </tr>
                                             </thead>
                                             <tbody>
                                                <?php $i=1; foreach ($complaints as $complain){ ?>
                                                   <tr id="<?php echo "complain_".$i; ?>">
                                                      <td><?php echo $i; ?></td> 
                                                      <td><?php echo $complain['complain']; ?></td>
                                                      <td><?php if(!empty($complain['complain_approve_by'])){ echo get_user_name( $complain['complain_approve_by'] ); } else { echo "-----"; } ?></td> 
                                                      <td> 
                                                         <?php if($complain['status'] == 1) { ?>
                                                             <span class="label label-primary">Approve</span>
                                                         <?php } elseif($complain['status'] == 2) { ?>
                                                             <span class="label label-worning">Pending</span>
                                                         <?php } elseif($complain['status'] == 3) { ?>
                                                             <span class="label label-danger">Cancel</span>
                                                         <?php } ?> 
                                                      </td>
                                                      <td>
                                                         <?php echo date('F jS, Y - h:i a' ,strtotime($complain['created_at'])); ?>  
                                                      </td>
                                                      <td>
                                                         <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $complain['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button> 
                                                     </td>
                                                 </tr>  
                                                   </tr> 
                                                <?php $i++; } ?>
                                             </tbody>
                                         </table>
                                     </div>
                                 </div>
                              </div>
                           </div>
                        </div> 
                     </div> 
                  </div>
               </div>  
            </div>
            <div class="modal inmodal" id="add_complain_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_complain__body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Vehicle</h5>
                    </div>
                    <form method="post" id="add_complain_form">
                        <div class="modal-body"> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Complain:</label>
                                    <input type="test" name="complain" id="complain" class="form-control" required="true">
                                </div>
                            </div> 
                        </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="ladda-button btn btn-primary" id="submit_complain" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
         <div class="modal inmodal" id="add_vehicle_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog" id="add_vehicles_body">
                      <div class="modal-content animated flipInY">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                              <h5 class="modal-title">Add New Vehicle</h5>
                          </div>
                          <form method="post" id="add_vehicle_form">
                              <div class="modal-body"> 
                                  <div class="row">
                                      <div class="form-group col-md-12">
                                          <label>Registration Number</label>
                                          <input type="test" name="reg_no" id="reg_no" class="form-control" required="true">
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="form-group col-md-12">
                                          <label>Brand:</label>
                                          <input type="test" name="company_name" id="company_name" class="form-control" required="true">
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="form-group col-md-12">
                                          <label>Model:</label>
                                          <input type="test" name="model" id="model" class="form-control" required="true">
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="form-group col-md-12">
                                          <label>Year Of Make:</label>
                                          <input type="number" name="year_of_make" id="year_of_make" class="form-control" required="true">
                                      </div>
                                  </div>
                              </div>
                              <div class="modal-footer"> 
                                  <button type="submit" class="ladda-button btn btn-primary" id="submit_vehicle" data-style="expand-right">Submit</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
</div> 

<?php $this->load->view('common/footer'); ?>

<script type="text/javascript">
   $('#vehicles-table').dataTable({
      "paging": true,
      "searching": true,
      "responsive": true,
      "order": [[ 7, 'asc' ]],
      "columnDefs": [
         { "responsivePriority": 1, "targets": 0 },
         { "responsivePriority": 2, "targets": -1 }
      ]
   });   
   $('#complain-table').dataTable({
      "paging": true,
      "searching": true,
      "responsive": true,
      "order": [[ 5, 'asc' ]],
      "columnDefs": [
         { "responsivePriority": 1, "targets": 0 },
         { "responsivePriority": 2, "targets": -1 }
      ]
   });
   $(document).on("submit" , "#add_complain_form" , function(e) {
   e.preventDefault();
   var formData = $("#add_complain_form").serialize(); 
    $.ajax({
        url:base_url+'welcome/submit_complain',
        type: 'POST',
        data: formData,
        dataType:'json',
        processData: false,
        success:function(status){
            if(status.msg=='success') {
               setTimeout(function () {
                    $(location).attr('href', base_url+'welcome/profile');
                }, 2000);
            } else if(status.msg == 'error') {
                toastr.error(status.response);
            }
        }
    });
});
   $(document).on("submit" , "#add_vehicle_form" , function(e) {
   e.preventDefault();
   var formData = $("#add_vehicle_form").serialize(); 
    $.ajax({
        url:base_url+'welcome/submit_vehicle',
        type: 'POST',
        data: formData,
        dataType:'json',
        processData: false,
        success:function(status){
            if(status.msg=='success') {
               setTimeout(function () {
                    $(location).attr('href', base_url+'welcome/profile');
                }, 2000);
            } else if(status.msg == 'error') {
                toastr.error(status.response);
            }
        }
    });
});
</script>


