<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller { 
	function __construct()
	{
		parent::__construct();		
		$this->load->library(admin_controller().'login_lib'); 
		$this->load->model(admin_controller().'vehicle_model'); 
	}

	public function index()
	{
		$this->load->view('index');
	}	
	public function profile()
	{
		$data['vehicles'] 	= $this->vehicle_model->get_vehicles(); 
		$data['complaints'] = get_data('','vehicle_complains',array('complain_by'=>get_session('admin_id'))); 
		$this->load->view('profile',$data);
		
	} 
	public function find_match()
	{
        if($_POST){
            $data = $_POST;
            $data['vehicles'] = get_data('','vehicles',array('reg_no'=>$data['reg_no']));
            if( count( $data['vehicles'] ) > '0' ) {

            	$vehicle['vehicles'] 		= $data['vehicles'][0];
            	$vehicle['veh_id']      	= $data['vehicles'][0]['id'];
            	$vehicle['vehicle_meta']   	= get_data('','vehicle_meta',array('vehicle_id'=>$vehicle['veh_id']));
            	$vehicle['meta_count']     	= count($vehicle['vehicle_meta']);
            	$htmlrespon = $this->load->view('view_search_ajax' , $vehicle,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
                exit;

            } else {
            	$vehicle['meta_count']     = '0';
            	$htmlrespon = $this->load->view('view_search_ajax' , $vehicle,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
            }
        }else{
            show_admin404();
        }

	}
	public function register_user()
	{
        if ($_POST){
            $data 				= $this->input->post();  
            $this->form_validation->set_rules('f_name', 'First name', 'required');
            $this->form_validation->set_rules('l_name', 'First name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm-password', 'Confirm Password', 'required|matches[password]');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
            	$email_check = get_data('','users',array('email'=>$data['email']));
                if (empty($email_check)) { 
	                $user_id = register_user($data);
	                if($user_id){
	                	if( $this->login_lib->validate_login($data['email'], $data['password'], '3') ) { 
							$finalResult = array('msg' => 'success');
			                echo json_encode($finalResult);
			                exit;  
						} else { 
			                $finalResult = array('msg' => 'error', 'response'=> 'Incorrect Email/Password or Combination');
			                echo json_encode($finalResult);
			                exit; 
						} 
	                }else { 
		                $finalResult = array('msg' => 'error', 'response' => "Something went wrong please check your internet connection.");
		                echo json_encode($finalResult);
		                exit;
	                } 
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Email already exist');
                    echo json_encode($finalResult);
                    exit;
                }

            }
        }else{
            show_admin404();
        }  
	}
	public function submit_complain()
	{
        if ($_POST){
            $data 				= $this->input->post();  
            $this->form_validation->set_rules('complain', 'Complain', 'required'); 
            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $insert_id = submit_complain($data);
                if($insert_id){
                	$finalResult = array('msg' => 'success');
	                echo json_encode($finalResult);
	                exit; 
                }else { 
	                $finalResult = array('msg' => 'error', 'response' => "Something went wrong, please check your internet connection.");
	                echo json_encode($finalResult);
	                exit;
                }
            }
        }else{
            show_admin404();
        }  
	}
	public function submit_vehicle()
	{
        if ($_POST){
            $data = $this->input->post();  
            $this->form_validation->set_rules('reg_no', 'Registration number', 'required'); 
            $this->form_validation->set_rules('company_name', 'Company name', 'required'); 
            $this->form_validation->set_rules('model', 'Model', 'required'); 
            $this->form_validation->set_rules('year_of_make', 'Year of make', 'required');  
            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $insert_id = submit_vehicle($data);
                if($insert_id){
                	$finalResult = array('msg' => 'success');
	                echo json_encode($finalResult);
	                exit; 
                }else { 
	                $finalResult = array('msg' => 'error', 'response' => "Something went wrong, please check your internet connection.");
	                echo json_encode($finalResult);
	                exit;
                }
            }
        }else{
            show_admin404();
        }  
	}

	public function login_user()
	{
        if ($_POST){
            $data = $this->input->post(); 
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
       			$email  	= trim($this->input->post('email'));
				$password   = trim($this->input->post('password'));

				if( $this->login_lib->validate_login($email, $password, '3') ) { 
					$finalResult = array('msg' => 'success');
	                echo json_encode($finalResult);
	                exit;  
				} else { 
	                $finalResult = array('msg' => 'error', 'response'=> 'Incorrect Email/Password or Combination');
	                echo json_encode($finalResult);
	                exit; 
				}
            }
        }else{
            show_admin404();
        } 
	}

	public function upload_csv()
	{

		ini_set('max_execution_time', 300);
	 	$count	=	0;
        $fp = fopen(FCPATH . 'assets\csv.csv','r') or die("can't open file");
        while($csv_line = fgetcsv($fp,1024))
        {
            $count++;
            if($count == 1)
            {
                continue;
            }
            // for($i = 0, $j = count($csv_line); $i < $j; $i++)
            // {
    	            $data = array(
		                'restaurant_name' => $csv_line['0'] ,
		                'restaurant_state' => $csv_line['1'] ,
		                'restaurant_city' => $csv_line['2'] ,
		                'restaurant_postal_code' => $csv_line['3'] ,
		                'restaurant_street' => $csv_line['4'] ,
		                'restaurant_website' => $csv_line['5'] ,
		                'menu_name' => $csv_line['6'] ,
		                'menu_section_name' => $csv_line['7'] ,
		                'menu_section_description' => $csv_line['8'] ,
		                'menu_item_name' => $csv_line['9'] ,
		                'menu_item_description' => $csv_line['10'] ,
		                'menu_item_image_name' => $csv_line['11'] ,
		                'menu_item_image_path' => $csv_line['12'] ,
		                'price' => $csv_line['12']
		               );
		            $this->db->insert('menu_items', $data);
        	// }
         }
         fclose($fp) or die("can't close file");
	}
}
