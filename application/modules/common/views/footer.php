<!-- Footer
		============================================= -->
<footer id="footer" class="dark">
    
    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">
        <div class="container clearfix">
            <div class="col_two_fifth nobottommargin">
                Copyrights © 2020 <strong class="text-white copyright-title"> Vehicle Workshop! </strong> All Rights Reserved.
            </div>
        </div>
    </div><!-- #copyrights end -->
</footer><!-- #footer end -->
</div><!-- #wrapper end -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/plugins.js"></script>
<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery.twbsPagination.js"></script>
<!-- Footer Scripts
============================================= -->
<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/functions.js"></script>
<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/components/bs-select.js"></script>
<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/components/select-boxes.js"></script>
<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>
</body>
</html>

