<?php
$r_class    = $this->router->fetch_class();
$r_method   = $this->router->fetch_method();
?>

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" style="width: 50px;" src="<?php echo base_url(); ?>assets/car.png"/>
                    <span data-toggle="dropdown" class="dropdown-toggle admin-name">
                        <span class="block m-t-xs font-bold">
                            Name: <?php echo get_session('admin_username'); ?>  
                        </span>
                        <span class="text-muted text-xs block">
                            Email: <?php echo get_session('admin_email'); ?>
                        </span>
                    </span>
                </div>
                <div class="logo-element">
                    Vehicle Workshop
                </div>
            </li>
            <li class="<?php if($r_class == 'admin' && ($r_method == 'dashboard' || $r_method == 'index')) { ?> active <?php } ?>">
                <a href="<?php echo admin_url(); ?>dashboard"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <?php if( get_session('admin_type') == '1') { ?>
                <li class="<?php if($r_class == 'requests' && $r_method == 'index') { ?> active <?php } ?>">
                    <a href="<?php echo admin_url(); ?>requests"><i class="fa fa-paper-plane"></i> <span class="nav-label">Requests</span></a>
                </li>
            <?php } ?>
            <?php if( get_session('admin_type') == '1' || get_session('admin_type') == '2') { ?>
                <li class="<?php if($r_class == 'user' && ($r_method == 'index' || $r_method == 'vehicle_users' || $r_method == 'mechanic_users')) { ?> active <?php } ?>">
                    <a href="<?php echo admin_url(); ?>user"><i class="fa fa-user"></i> <span class="nav-label">Users</span>
                        <?php if( get_session('admin_type') != '1') { ?>
                            <span class="fa arrow"></span>
                        <?php } ?>
                    </a>
                    <?php if( get_session('admin_type') != '1') { ?>
                        <ul class="nav nav-second-level collapse">
                            <li class="<?php if($r_class == 'user' && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>user">All Users</a></li>
                            <li class="<?php if($r_class == 'user' && ($r_method == 'vehicle_users')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>user\vehicle_users">Vehicle Owners</a></li>
                            <li class="<?php if($r_class == 'user' && ($r_method == 'mechanic_users')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>user\mechanic_users">Mechanics</a></li>
                        </ul>
                    <?php } ?>
                </li>
                <li class="<?php if(($r_class == 'vehicle' || $r_class == 'invoice') && ($r_method == 'index' || $r_method == 'invoice' || $r_method == 'add_invoice')) { ?> active <?php } ?>">
                    <a href="<?php echo admin_url(); ?>vehicle"><i class="fa fa-car"></i> 
                        <span class="nav-label">Vehicles</span><span class="fa arrow"></span>
                    </a>
                     <ul class="nav nav-second-level collapse">
                        <li class="<?php if( ($r_class == 'vehicle' || $r_class == 'admin') && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>vehicle"><i class="fa fa-car"></i> Vehicles </a></li>
                        <li class="<?php if($r_class == 'invoice' && ( $r_method == 'index' || $r_method == 'add_invoice' )) { ?> active <?php } ?>">
                            <a href="<?php echo admin_url(); ?>invoice"><i class="fa fa-file"></i> <span class="nav-label">Invoice</span></a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'index' || $r_method == 'sub_parts' || $r_method == 'invoice' || $r_method == 'add_invoice')) { ?> active <?php } ?>">
                     <a href="<?php echo admin_url(); ?>spare_parts"><i class="fa fa-industry"></i> <span class="nav-label">Spare Parts</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>spare_parts">Categories </a></li>
                        <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'sub_parts')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>spare_parts\sub_parts">Products </a></li>
                        <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'invoice' || $r_method == 'add_invoice')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>spare_parts\invoice">Invoices</a></li>
                    </ul>
                </li>
                
            <?php } elseif ( get_session('admin_type') == '4' ) { ?>
                <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'index' || $r_method == 'sub_parts' || $r_method == 'invoice' || $r_method == 'add_invoice')) { ?> active <?php } ?>">
                     <a href="<?php echo admin_url(); ?>spare_parts"><i class="fa fa-industry"></i> <span class="nav-label">Spare Parts</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'index')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>spare_parts">Categories </a></li>
                        <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'sub_parts')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>spare_parts\sub_parts">Products </a></li>
                        <li class="<?php if($r_class == 'spare_parts' && ($r_method == 'invoice')) { ?> active <?php } ?>"><a href="<?php echo admin_url(); ?>spare_parts\invoice">Invoices  (Coming Soon)</a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if( get_session('admin_type') != '1') { ?>
            <li class="<?php if($r_class == 'Feedback' && $r_method == 'index') { ?> active <?php } ?>">
                <a href="<?php echo admin_url(); ?>Feedback"><i class="fa fa-comments-o"></i> <span class="nav-label">Feedback</span></a>
            </li>
            <?php } ?> 
            <li class="<?php if($r_class == 'statistics' && $r_method == 'index') { ?> active <?php } ?>">
                <a href="<?php echo admin_url(); ?>statistics"><i class="fa fa-bar-chart"></i> <span class="nav-label">Statistics </span></a>
            </li>
            <li class="<?php if($r_class == 'expenses' && $r_method == 'index') { ?> active <?php } ?>">
                <a href="<?php echo admin_url(); ?>expenses"><i class="fa fa-money"></i> <span class="nav-label">Expenses </span></a>
            </li>
        </ul>
    </div>
</nav>