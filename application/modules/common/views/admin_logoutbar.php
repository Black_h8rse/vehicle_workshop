<nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javscript:void();"><i class="fa fa-bars"></i> </a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown notifications-menu">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <!-- <span class="label label-warning"></span> -->
            </a>
            <ul class="dropdown-menu" style="width: 500px;">
                <li class="header" style="text-align: center; font-size: 20px;">Notifications</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu" style="min-height: 300px; padding: 0px; height: 110px; overflow: scroll;">
                        <?php foreach ( get_table('vehicle_complains') as $complain) { if( $complain['status'] == '2' ) { ?> 
                            <li>
                                <a href="javascript:void(0)">
                                    <div class="row" style="padding: 15px;">
                                        <div class="col-md-1">
                                            <i class="fa fa-warning text-yellow"></i> 
                                        </div>
                                        <div class="col-md-11"> 
                                            <p> <?php echo $complain['complain'];?></p>
                                            <button style="float: right" class="btn btn-success accept-btn" data-id="<?php echo $complain['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-check"></i> Accept</button>
                                        </div>
                                    </div>
                                </a>
                            </li> 
                            <hr>
                        <?php } } ?>
                        </ul>
                    </li>
                <li class="footer"><a style="text-align: center;" href="javascript:void(0)"></a></li>
            </ul>
        </li>
        <li>
            <span class="m-r-sm text-muted welcome-message">Welcome to Vehicle Workshop </span>
        </li>
        <li>
            <a href="<?php echo admin_url(); ?>change_password">
                <i class="fa fa-key"></i> Change Password
            </a>
        </li>
        <li>
            <a href="<?php echo admin_url(); ?>logout">
                <i class="fa fa-sign-out"></i> Logout
            </a>
        </li>
    </ul>
</nav> 