<!DOCTYPE html>

<html dir="ltr" lang="en-US">

<head>





    <?php
    $Url = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    $Url .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    ?>

    <!-- Stylesheets -->


    <!-- add icon link -->
    <link rel = "icon" href ="<?php echo base_url(); ?>assets/car.png" type = "image/x-icon">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.png">


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/bootstrap.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/style.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/swiper.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/dark.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-icons.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/animate.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/magnific-popup.css" type="text/css" />



    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/responsive.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/custom.css" type="text/css" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/components/bs-select.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/components/select-boxes.css" type="text/css" />

    <!-- Data Table -->
<link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/responsive.bootstrap4.min.css" rel="stylesheet">


    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
</head>



<body class="stretched dark sticky-responsive-menu" >



<!-- Document Wrapper

============================================= -->

<div id="wrapper" class="clearfix">



    <!-- Header

    ============================================= -->

    <header id="header" class=" dark full-header custom-header transparent-header semi-transparent sticky-nav-mobile" >
        <!--  -->
    </header><!-- #header end -->

