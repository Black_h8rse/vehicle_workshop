<!-- contact us ============================================== -->
<div class="clearfix scarl_contact">

    <div class="col-md-6 center nopadding">
        <div>
            <div id="map" class="custom-height text-dark"></div>
        </div>
    </div>

    <div class="col-md-6 text-left col-padding ">
        <div class="contact_us">
            <div class="heading-block">
                <h3 class="nobottommargin text-size-14"><?php echo get_setting('contact_detail','first_statement'); ?></h3>
            </div>
            <p>
                <strong>
                    <?php echo get_setting('contact_detail','middle_statement'); ?>
                </strong>
            </p>
            <div class="clearfix">
                <div class="col_half">
                    <div class="feature-box fbox-effect">
                        <div class="fbox-icon">
                            <a href="tel:<?php echo get_setting('contact_detail','phone_no'); ?>"><i class="icon-phone i-alt"></i></a>
                        </div>
                        <h3>
										<span>
									        Phone:
									    </span>
                        </h3>
                        <p class="scarlet_contact">
                            <a href="tel:<?php echo get_setting('contact_detail','phone_no'); ?>">
                                <?php echo get_setting('contact_detail','phone_no'); ?>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col_half col_last">
                    <div class="feature-box fbox-effect">
                        <div class="fbox-icon">
                            <a href="mailto:<?php echo get_setting('contact_detail','email'); ?>"><i class="icon-envelope i-alt"></i></a>
                        </div>
                        <h3>
										<span>
									        Email:
									    </span>
                        </h3>
                        <p class="scarlet_contact">
                            <a href="mailto:<?php echo get_setting('contact_detail','email'); ?>">
                                <?php echo get_setting('contact_detail','email'); ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <p class="p-head text-size-18">
                <?php echo get_setting('contact_detail','bottom_statement'); ?>
            </p>
            <div class="col_full text-left nobottommargin">
                <a href="<?php echo base_url(); ?>contact-us" class="button nomargin">
                    Contact Us
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>

<!-- contact us end -->
