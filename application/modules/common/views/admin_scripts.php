<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Toastr script -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/switchery/switchery.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/ckeditor/ckeditor.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/admin_assets/ckeditor/config.js"></script> -->


<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/pace/pace.min.js"></script>

<!-- Sweet alert -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin_assets/js/jquery.maskedinput.min.js"></script>
<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>




<div class="modal inmodal" id="customer_detail_model" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content animated flipInY">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h5 class="modal-title">Complain Detail</h5>
        </div>
        <div class="modal-body customer_detail_body">
          
        </div>
       <div class="modal-footer"></div>
       <p>Call direct on customer phone number</p>
      </div>
    </div>
</div> 

<script type="text/javascript">  
    $(document).on("click" , ".accept-btn" , function(e) {
        e.preventDefault();  
        var complain_id = $(this).attr("data-id"); 
        console.log(complain_id);
        $.ajax({
            url:admin_url+'admin/get_complain_customer_detail',
            type: 'POST',
            data: {complain_id : complain_id},
            dataType:'json', 
            success:function(status){ 
                $(".customer_detail_body").html(status.response); 
                $("#customer_detail_model").modal('show');
            }
        });
    }); 
</script>