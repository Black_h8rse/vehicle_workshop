<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title> Vehicle Workshop | Admin Panel</title>




<!-- add icon link -->
<link rel = "icon" href ="<?php echo base_url(); ?>assets/car.png" type = "image/x-icon">

<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.png">

<link href="<?php echo base_url(); ?>assets/admin_assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/font-awesome/css/font-awesome.css" rel="stylesheet">

<!-- Sweet Alert -->
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/switchery/switchery.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<!-- Toastr style -->
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/custom.css" rel="stylesheet">

<!-- Ladda style -->
<link href="<?php echo base_url(); ?>assets/admin_assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<!-- Data Table -->
<link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/responsive.bootstrap4.min.css" rel="stylesheet">

<script>

    var base_url = "<?php echo base_url(); ?>";

    var admin_url = "<?php echo admin_url(); ?>";

</script>