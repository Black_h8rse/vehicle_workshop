<!-- logo slider
				==================================== -->
<section class="carousel_footer nomargin nopadding">

    <div id="oc-clients-full" class="brands-carousel owl-carousel image-carousel carousel-widget pt4 pb4" data-margin="30" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false"data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="5" data-items-lg="6">

        <?php foreach (get_carousel() as $carousel){ ?>
            <div class="oc-item">
                <a href="<?php echo $carousel['url']; ?>"><img src="<?php echo base_url(); ?>assets/carousel/<?php echo $carousel['image']; ?>" alt="<?php echo $carousel['image_alt']; ?>"></a>
            </div>
        <?php } ?>

    </div>
</section>
<!-- logo slider end -->