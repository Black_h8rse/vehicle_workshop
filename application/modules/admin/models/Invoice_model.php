<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_invoices()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('invoices');
        $this->db->where('type', "vehicle");
        return $this->db->get()->result_array();
    }
    public function insert_invoice($data)
    { 
        if( $data['customer_type'] == '1' ) { 
            $this->db->set('type', "vehicle");  
            $this->db->set('vehicle_id', $data['vehicle']['0']['id']);
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->set('user_id', $data['vehicle']['0']['user_id']);
            $this->db->set('paid_price', $data['paid_amount']);
            $this->db->set('due_price', $data['due_price_value'] );
            $this->db->set('total_price', $data['total_price_value']);
            $this->db->set('rendom_no', $data['rendom_key']);
            $this->db->insert('invoices');
            $invoice_id = $this->db->insert_id(); 
            foreach ($data['meta'] as $value) {
                $this->db->set('invoice_id', $invoice_id);
                $this->db->set('meta_key', $value['key']);
                $this->db->set('meta_value', $value['value']);
                $this->db->set('price', $value['value']);
                $this->db->insert('invoice_meta');
            } 
        } else {

            $this->db->set('type', 'vehicle');
            $this->db->set('paid_price', $data['paid_amount']);
            $this->db->set('due_price', $data['due_price_value'] );
            $this->db->set('total_price', $data['total_price_value']);
            $this->db->set('rendom_no', $data['rendom_key']);
            $this->db->set('vehicle_id', 'NEW');
            $this->db->set('user_id', 'NEW');
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->insert('invoices');
            $invoice_id =  $this->db->insert_id(); 
            $this->db->set('invoice_id', $invoice_id);
            $this->db->set('meta_key', 'name');
            $this->db->set('meta_value', $data['name']);
            $this->db->insert('invoice_meta');
            $this->db->set('invoice_id', $invoice_id);
            $this->db->set('meta_key', 'phone');
            $this->db->set('meta_value', $data['phone']);
            $this->db->insert('invoice_meta');
            $this->db->set('invoice_id', $invoice_id);
            $this->db->set('meta_key', 'vehicle_reg_no');
            $this->db->set('meta_value', $data['v_reg_no']);
            $this->db->insert('invoice_meta');
            $this->db->set('invoice_id', $invoice_id);
            $this->db->set('meta_key', 'address');
            $this->db->set('meta_value', $data['address']);
            $this->db->insert('invoice_meta');
            foreach ($data['meta'] as $value) {
                $this->db->set('invoice_id', $invoice_id);
                $this->db->set('meta_key', $value['key']);
                $this->db->set('meta_value', $value['value']);
                $this->db->insert('invoice_meta');
            }

        }
        return $invoice_id;
    }
    public function delete_invoice($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('invoices');


        $this->db->where('invoice_id', $id);
        $this->db->delete('invoice_meta');
        return $this->db->affected_rows();


    }
    public function get_invoice_details($data)
    {
        $this->db->select("*");
        $this->db->from("invoices");
        $this->db->where('id',$data['id']);
        $query = $this->db->get();
        return $query->row_array();
    }    
    public function get_invoice_meta_details($data)
    {
        $this->db->select("*");
        $this->db->from("vehicle_meta");
        $this->db->where('vehicle_id',$data['veh_id']);
        return $this->db->get()->result_array();
    }
    public function update_invoice($data)
    {
        $this->db->set('user_id', $data['user_id']);
        $this->db->set('company_name', $data['company_name']);
        $this->db->set('reg_no', $data['reg_no']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->where('id', $data['id']);
        $query = $this->db->update('invoices');
        return $this->db->affected_rows();
    }
    public function insert_invoice_meta($data,$veh_id)
    {
        $this->db->where('vehicle_id',  $veh_id);
        $this->db->delete('vehicle_meta');

        foreach ($data as $key => $value) {
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->set('vehicle_id', $veh_id);
            $this->db->set('meta_key', $value['key']);
            $this->db->set('meta_value', $value['value']);
            $this->db->set('rendom_key', "for_future_use");
            $this->db->insert('vehicle_meta');
        }
        return $this->db->insert_id();
    }
}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/admin_model.php */