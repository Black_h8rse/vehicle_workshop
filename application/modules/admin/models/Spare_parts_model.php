<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spare_parts_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_spare_parts()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('material_cat');
        return $this->db->get()->result_array();
    }
    public function insert_invoice($data)
    { 
        $this->db->set('type', 'spare_part');
        $this->db->set('paid_price', $data['paid_amount']);
        $this->db->set('due_price', $data['due_price_value'] );
        $this->db->set('total_price', $data['total_price_value']);
        $this->db->set('vehicle_id', 'sparepart');
        $this->db->set('user_id', 'sparepart');
        $this->db->set('rendom_no', $data['rendom_key']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('invoices');
        $invoice_id = $this->db->insert_id(); 
        $this->db->set('invoice_id', $invoice_id);
        $this->db->set('meta_key', 'name');
        $this->db->set('meta_value', $data['name']);
        $this->db->insert('invoice_meta');
        $this->db->set('invoice_id', $invoice_id);
        $this->db->set('meta_key', 'phone');
        $this->db->set('meta_value', $data['phone']);
        $this->db->insert('invoice_meta');
        $this->db->set('invoice_id', $invoice_id);
        $this->db->set('meta_key', 'address');
        $this->db->set('meta_value', $data['address']);
        $this->db->insert('invoice_meta');  
        foreach ($data['meta'] as $value) {
            if( @!empty($value['value']) ){
                $this->db->set('invoice_id', $invoice_id);
                $this->db->set('meta_key', $value['key']);
                $this->db->set('meta_value', 'null');
                $this->db->set('price', $value['value']);
                $this->db->insert('invoice_meta'); 
            } else { 
                $left_quantity  = $value['quantity_enter_max'] - $value['quantity_enter'];
                $this->db->set('quantity', $left_quantity); 
                $this->db->where('id', $value['key']);
                $this->db->update('material_pro');
                $this->db->set('invoice_id', $invoice_id);
                $this->db->set('meta_key', $value['key']);
                $this->db->set('meta_value', $value['quantity_enter']);
                $this->db->set('price', $value['amount_enter']);
                $this->db->insert('invoice_meta');
            }
        }
        return $invoice_id;
    }
    public function get_invoices()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('invoices');
        $this->db->where('vehicle_id', 'sparepart');
        $this->db->where('type', 'spare_part');
        return $this->db->get()->result_array();
    }
    public function get_sub_parts()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('material_pro');
        return $this->db->get()->result_array();
    }
    public function insert_category($data)
    {
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('name', $data['name']);
        $this->db->set('description', $data['description']);
        $this->db->insert('material_cat');
        return $this->db->insert_id();
    }   
    public function insert_sub_category($data)
    {
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('name', $data['name']);
        $this->db->set('quantity', $data['quantity']);
        $this->db->set('buy_price', $data['buy_price']);
        $this->db->set('material_id', $data['cat_id']);
        $this->db->set('sale_price', '0');
        $this->db->insert('material_pro');

        if( $this->db->insert_id() ) {
            $this->db->set('meterial_pro_id', $this->db->insert_id());
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->set('meta_key', 'price_per_piece');
            $this->db->set('meta_value', $data['price_per_piece']);
            $this->db->insert('material_meta');
        }
        return $this->db->insert_id();
    }
    public function delete_category($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('material_cat');

        $this->db->where('material_id', $id);
        $this->db->delete('material_pro');

        return $this->db->affected_rows();
    }
    public function delete_sub_category($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->delete('material_pro');
        return $this->db->affected_rows();
    }
    public function get_category_details($data)
    {
        $this->db->select("*");
        $this->db->from("material_cat");
        $this->db->where('id',$data['id']);
        $query = $this->db->get();
        return $query->row_array();
    }  
    public function get_category_products($data)
    {
        $this->db->select("*");
        $this->db->from("material_pro");
        $this->db->where('material_id',$data['id']);
        return $this->db->get()->result_array();
    }   
    public function get_vehicle_meta_details($data)
    {
        $this->db->select("*");
        $this->db->from("material_cat");
        $this->db->where('vehicle_id',$data['veh_id']);
        return $this->db->get()->result_array();
    }
    public function update_category($data)
    {
        $this->db->set('description', $data['description']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->where('id', $data['cat_id']);
        $query = $this->db->update('material_cat');
        return $this->db->affected_rows();
    }
    public function update_product_quantity($data)
    {
        $this->db->set('quantity', $data['pro_id_old_quantity']+$data['add_quantity']);
        $this->db->where('id', $data['pro_id_add_quantity']);
        $query = $this->db->update('material_pro');

        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('pro_id', $data['pro_id_add_quantity']);
        $this->db->set('meta_key', 'Purchased');
        $this->db->set('meta_value', $data['add_quantity']);
        $this->db->insert('material_description');


        $this->db->set('expense_type_id', '3');
        $this->db->set('meta_key', "Purchase Products"); 
        $this->db->set('meta_value', $data['add_price']); 
        $this->db->set('detail', "Purchase Products"); 
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('expenses');




        $this->db->set('meterial_pro_id', $query);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('meta_key', 'price_per_piece');
        $this->db->set('meta_value', $data['add_per_piece_price']);
        $this->db->insert('material_meta');
        
        return $this->db->insert_id();

    }
    public function product_sold($data)
    {
        $this->db->set('quantity', $data['quantity_to_sell_max']-$data['quantity_to_sell']);
        $this->db->where('id', $data['pro_id']);
        $query = $this->db->update('material_pro');

        $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('pro_id', $data['pro_id']);
        $this->db->set('meta_key', 'sold');
        $this->db->set('meta_value', $data['quantity_to_sell']);
        $query2 = $this->db->insert('material_description');

        return $this->db->insert_id();
    }
    public function insert_vehicle_meta($data,$veh_id)
    {
        $this->db->where('vehicle_id',  $veh_id);
        $this->db->delete('material_cat');

        foreach ($data as $key => $value) {
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->set('vehicle_id', $veh_id);
            $this->db->set('meta_key', $value['key']);
            $this->db->set('meta_value', $value['value']);
            $this->db->set('rendom_key', "for_future_use");
            $this->db->insert('vehicle_meta');
        }
        return $this->db->insert_id();
    }
}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/admin_model.php */