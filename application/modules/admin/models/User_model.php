<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_users()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("id,f_name,l_name,email,phone,user_type,created_by,created_at");
        $this->db->from('users');
        return $this->db->get()->result_array();
    }
    public function delete_user($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('users');

        $this->db->set('created_by', '1');
        $this->db->where('created_by', $id);
        $this->db->update('vehicles');

        $this->db->set('created_by', '1');
        $this->db->where('created_by', $id);
        $this->db->update('vehicle_meta');

        $this->db->set('created_by', '1');
        $this->db->where('created_by', $id);
        $this->db->update('requests');

        $this->db->set('created_by', '1');
        $this->db->where('created_by', $id);
        $this->db->update('material_cat');

        $this->db->set('created_by', '1');
        $this->db->where('created_by', $id);
        $this->db->update('material_pro');

        $this->db->where('created_by', $id);
        $this->db->delete('invoices');

        $this->db->where('created_by', $id);
        $this->db->delete('feedback');

        $this->db->set('created_by', '1');
        $this->db->where('created_by', $id);
        $this->db->update('users');


        return $this->db->affected_rows();



    }
    public function insert_user($data)
    {
        $this->db->set('f_name', $data['f_name']);
        $this->db->set('l_name', $data['l_name']);
        $this->db->set('phone', $data['phone']);
        $this->db->set('email', $data['email']);
        $this->db->set('password', hash( 'sha256', $data['password'] ) );
        $this->db->set('created_by', get_session('admin_id'));
        // 1=superadmin,2=workshop_admin,3=vehicle_owners,4=spare_part_shop_owners,5=mechanic
        if( get_session('admin_type') == '2' || get_session('admin_type') == '4') {
            if( $data['user_type'] == '1' ) {
                $this->db->set('user_type', '3');
            } elseif ( $data['user_type'] == '2' ) {
                $this->db->set('user_type', '5');
            } else {
                $this->db->set('user_type', '6');
            }
        } elseif ( get_session('admin_type') == '1' ) {
            if( $data['user_type'] == '1' ) {
                $this->db->set('user_type', '2');
            } elseif ( $data['user_type'] == '2' ) {
                $this->db->set('user_type', '5');
            } else {
                $this->db->set('user_type', '4');
            }
        }
        $this->db->insert('users');
        return $this->db->insert_id();
    }
    public function get_user_details($data)
    {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('id',$data['user_id']);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function update_user($data)
    {
        $this->db->set('f_name', $data['f_name']);
        $this->db->set('l_name', $data['l_name']);
        $this->db->set('phone', $data['phone']);
        $this->db->where('id', $data['user_id']);
        $query = $this->db->update('users');
        return $this->db->affected_rows();
    }
}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/admin_model.php */