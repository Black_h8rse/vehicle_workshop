<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_result($data) { 
        $this->db->select("*");
        $this->db->from('invoices'); 
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        } 
        if ($data['type'] == '1') { 
        } elseif ($data['type'] == '2') { 
            $this->db->where('type', "vehicle");
        } elseif ($data['type'] == '3') { 
            $this->db->where('type', "spare_part");
        } 
        if (!empty($data['s_date'])) { $this->db->where('Date(created_at) >=', date("Y-m-d", strtotime($data['s_date']))); }
        if (!empty($data['e_date'])) { $this->db->where('Date(created_at) <=', date("Y-m-d", strtotime($data['e_date']))); } 
        return $this->db->get()->result_array();
    }
   
}

/* End of file Statistics_model.php */
   /* Location: ./application/modules/admin/models/Statistics_model.php */