<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get()
    {
        $this->db->select("*");
        $this->db->from('requests');
        return $this->db->get()->result_array();
    }
    public function status_update($id,$value)
    {
        $this->db->set('status', $value);
        $this->db->where('id', $id);
        $query = $this->db->update('requests');
        return $this->db->affected_rows();
    }
    public function delete_request($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->delete('requests');
        return $this->db->affected_rows();
    }

}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/Request_model.php */