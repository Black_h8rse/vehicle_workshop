<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_feedback()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('feedback');
        return $this->db->get()->result_array();
    }
    public function delete($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->delete('feedback');
        return $this->db->affected_rows();
    }
    public function insert($data)
    {
        $this->db->set('text', $data['feedback']);
        $this->db->set('status', 'pending');
        $this->db->set('created_by', get_session('admin_id'));
        $feedback_id = $this->db->insert('feedback');

        if(!empty($feedback_id)) {
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->set('type', 'feedback');
            $this->db->set('status', 'pending');
            $requests_id = $this->db->insert('requests');

            $this->db->set('request_id', $requests_id);
            $this->db->set('meta_key', 'text');
            $this->db->set('meta_value', $data['feedback']);
            $this->db->insert('request_meta'); 
            return $this->db->insert_id();
        }

        

       


    }
    public function get_user_details($data)
    {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('id',$data['user_id']);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function update_user($data)
    {
        $this->db->set('f_name', $data['f_name']);
        $this->db->set('l_name', $data['l_name']);
        $this->db->set('phone', $data['phone']);
        $this->db->where('id', $data['user_id']);
        $query = $this->db->update('users');
        return $this->db->affected_rows();
    }
}

/* End of file Feedback_model.php */
   /* Location: ./application/modules/admin/models/Feedback_model.php */