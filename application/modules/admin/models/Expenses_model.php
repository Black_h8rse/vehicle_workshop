<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
    public function get_expenses()
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('expenses');
        return $this->db->get()->result_array();
    }
	public function add_expense_type($data) { 
         $this->db->set('created_by', get_session('admin_id'));
        $this->db->set('name', $data['name']);
        $this->db->set('status', '1'); 
        $this->db->insert('expense_type');
        return $this->db->insert_id();
    }
    public function add_expense($data) { 
        $this->db->set('expense_type_id', $data['expense_type_id']);
        $this->db->set('meta_key', "Expense"); 
        $this->db->set('meta_value', $data['price']); 
        $this->db->set('detail', $data['detail']); 
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('expenses');
        return $this->db->insert_id();
    }
    public function delete_expense($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('expenses');  
        return $this->db->affected_rows(); 
    }
   
}

/* End of file Expenses_model.php */
   /* Location: ./application/modules/admin/models/Expenses_model.php */