<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	   		//Do your magic here
	}
	public function get_vehicles($id='')
    {
        if( get_session('admin_id') != '1' ) {
            $this->db->where('created_by', get_session('admin_id'));
        }
        $this->db->select("*");
        $this->db->from('vehicles');
        if(!empty($id)){
            $this->db->where('user_id',$id); 
        }
        return $this->db->get()->result_array();
    }
    public function insert_vehicles($data)
    {
        $this->db->set('user_id', $data['user_id']);
        $this->db->set('company_name', $data['company_name']);
        $this->db->set('reg_no', $data['reg_no']);
        $this->db->set('model', $data['model']);
        $this->db->set('year_of_make', $data['year_of_make']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->insert('vehicles');
        return $this->db->insert_id();
    }
    public function delete_vehicle($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->delete('vehicles');
        return $this->db->affected_rows();
    }
    public function get_vehicle_details($data)
    {
        $this->db->select("*");
        $this->db->from("vehicles");
        $this->db->where('id',$data['id']);
        $query = $this->db->get();
        return $query->row_array();
    } 
    public function get_invoices_details($data)
    {
        $this->db->select("*");
        $this->db->from("invoices");
        $this->db->where('vehicle_id',$data['id']);
        return $this->db->get()->result_array();
    }   
    public function get_vehicle_meta_details($data)
    {
        $this->db->select("*");
        $this->db->from("vehicle_meta");
        $this->db->where('vehicle_id',$data['veh_id']);
        return $this->db->get()->result_array();
    }
    public function update_vehicle($data)
    {
        $this->db->set('user_id', $data['user_id']);
        $this->db->set('company_name', $data['company_name']);
        $this->db->set('reg_no', $data['reg_no']);
        $this->db->set('model', $data['model']);
        $this->db->set('year_of_make', $data['year_of_make']);
        $this->db->set('created_by', get_session('admin_id'));
        $this->db->where('id', $data['id']);
        $query = $this->db->update('vehicles');
        return $this->db->affected_rows();
    }
    public function insert_vehicle_meta($data,$veh_id)
    {
        $this->db->where('vehicle_id',  $veh_id);
        $this->db->delete('vehicle_meta');

        foreach ($data as $key => $value) {
            $this->db->set('created_by', get_session('admin_id'));
            $this->db->set('vehicle_id', $veh_id);
            $this->db->set('meta_key', $value['key']);
            $this->db->set('meta_value', $value['value']);
            $this->db->set('rendom_key', "for_future_use");
            $this->db->insert('vehicle_meta');
        }
        return $this->db->insert_id();
    }
}

/* End of file admin_model.php */
   /* Location: ./application/modules/admin/models/admin_model.php */