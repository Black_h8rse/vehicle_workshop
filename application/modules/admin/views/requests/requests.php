<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Requests</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Requests</strong>
                    </li>
                </ol>
            </div>
            
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Request list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="admin-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <th>Message</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($requests as $request){ ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>

                                            <td>
                                                <?php if($request['type'] == 'feedback') { ?>
                                                    <?php print_r( get_meta('request_meta','request_id',$request['id'],'meta_key','text') ); ?>
                                                <?php } ?>
                                                
                                                    
                                            </td>

                                            <td>
                                                <div class="dropdown">
                                                <?php if($request['status'] == 'pending') { ?>
                                                  <button class=" btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Pending
                                                        
                                                <?php } elseif($request['status'] == 'success') { ?>
                                                    <button class=" btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Success
                                                <?php } elseif($request['status'] == 'decline') { ?>
                                                    <button class=" btn btn-danger dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Decline
                                                <?php } ?>

                                                    </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" id="dropdown_select" data-value="pending" data-id="<?php echo $request['id']; ?>">Pending</a>
                                                    <a class="dropdown-item" id="dropdown_select" data-value="success" data-id="<?php echo $request['id']; ?>">Success</a>
                                                    <a class="dropdown-item" id="dropdown_select" data-value="decline" data-id="<?php echo $request['id']; ?>">Decline</a>
                                                  </div>
                                                </div>


                                               <!--  <?php if($request['status'] == 'pending') { ?>
                                                    <span class="label label-primary">Pending</span>
                                                <?php } elseif($request['status'] == 'success') { ?>
                                                    <span class="label label-success">Success</span>
                                                <?php } elseif($request['status'] == 'decline') { ?>
                                                    <span class="label label-danger">Decline</span>
                                                <?php } ?> -->
                                            </td>
                                            <td> <?php echo date('F jS, Y - h:i a' ,strtotime($request['created_at'])); ?> </td>  
                                            <td>
                                                <button class="btn btn-success btn-circle view-btn" data-id="<?php echo $request['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_feedback_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Feedback</h5>
                    </div>
                    <form method="post" id="add_feedback_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Feedback</label>
                                    <textarea class="form-control" id="feedback" name="feedback" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_feedback" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>

<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>



</body>
</html>

<script>    
    $('#admin-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'desc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });

    $(document).on('click', '#dropdown_select', function (event) {
        var value   = $(this).attr('data-value');
        var id      = $(this).attr('data-id');
        $.ajax({
            url:admin_url+'requests/status_update',
            type:'post',
            data:{ id : id,value : value },
            dataType:'json',
            success:function(status){
                toastr.success(status.response, 'Success');
                setTimeout(function () {
                    $(location).attr('href', admin_url+"requests");
                }, 2000);
            }
        });

    });


    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'feedback/delete',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"feedback");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , "#submit_feedback" , function() {

        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_feedback_form").serialize();
        $.ajax({
            url:admin_url+'feedback/save',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_feedback_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_feedback_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
</script>

