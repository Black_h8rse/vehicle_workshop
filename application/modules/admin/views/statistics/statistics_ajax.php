
<div class="table-responsive">
    <table id="result-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
        <tr>
            <th>Sr#</th>
            <?php if(get_session('admin_id') == '1'){?>
                <th>Created By</th>
            <?php } ?>
            <th>User Name</th>
            <th>Invoice Number</th>
            <th>Total amount</th>
            <th>Paid amount</th>
            <th>Due amount</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1; $due_payments = '0'; foreach ($result as $invoice){ ?>
            <tr id="<?php echo "row_".$i; ?>">
                <td><?php echo $i; ?></td>
                <?php if( get_session('admin_id') == '1' ) {  ?>
                    <?php if( $invoice['created_by'] != '0' ) {  ?>
                        <td> <span class="label label-success"><?php echo get_user_name( $invoice['created_by'] ); ?></span>  </td>
                    <?php }else{ ?>
                        <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                    <?php } ?>
                <?php } ?>
                <?php if( $invoice['user_id'] != 'NEW' && $invoice['user_id'] > '0') {  ?>
                    <td><?php echo get_user_name( $invoice['user_id'] ); ?></td>
                <?php } else { ?>
                    <td><?php echo 'Random Visitor'; ?></td>
                <?php } ?>
                <td><?php echo $invoice['rendom_no']; ?></td>
                <td><?php echo $invoice['total_price']; ?></td>
                <td><?php echo $invoice['paid_price']; ?></td>
                <td><?php echo $invoice['due_price']; $due_payments = $due_payments+$invoice['due_price'];?></td>
                <td><?php echo date('F jS, Y - h:i a' ,strtotime($invoice['created_at'])); ?></td>  
                <td>
                    <button class="btn btn-primary btn-circle" data-id="<?php echo $invoice['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                </td>
            </tr>
            <?php $i++; } ?>
        </tbody>
    </table>
</div>
<div class="row col-md-12">
    <div class="col-md-6" style="float: right;"></div>
    <p>Due Payments: <span><?php echo $due_payments;?></span></p>
</div> 


<script type="text/javascript">
    $('#result-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'asc' ]],
        "columnDefs": [
        { "responsivePriority": 1, "targets": 0 },
        { "responsivePriority": 2, "targets": -1 }
        ]
    });
</script>