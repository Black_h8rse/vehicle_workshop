<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Statistics</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Statistics</strong>
                    </li>
                </ol>
            </div>
            
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Search Form</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <div class="modal-body">
                                    <form method="post" id="search_form">
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label>Type:</label>
                                                <select class="form-control" name="type" id="type" required="true">
                                                    <option value="0">Please select type</option>
                                                    <option value="1">All</option>
                                                    <option value="2">Workshop</option>
                                                    <option value="3">Spare-Part</option>
                                                </select> 
                                            </div>
                                            
                                        </div>  
                                        <div class="row"> 
                                            <div class="form-group col-md-6">
                                              <label>Starting Date <span style="color: gray;">( Optional )</span> </label>
                                              <input type="date" name="s_date" parsley-trigger="change" placeholder="Please enter joining date" class="form-control">
                                            </div>  
                             
                                            <div class="form-group col-md-6">
                                              <label>Ending Date <span style="color: gray;">( Optional )</span> </label>
                                              <input type="date" name="e_date" parsley-trigger="change" placeholder="Please enter joining date" class="form-control">
                                            </div>  
                                        </div>
                                        <button type="button" class="btn btn-primary pull-right t_m_25" id="submit_form">Submit</button>
                                    </form> 
                                </div>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight search_result_div" style="display: none;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Search Result</h5>
                        </div>
                        <div class="ibox-content" style="padding: 0px;">
                            <div class="table-responsive">
                                <div class="modal-body search_result_body">
                                     
                                </div>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>


<script type="text/javascript">
 
    $(document).on("click" , "#submit_form" , function() { 
        console.log("yes"); 
        var formData = $("#search_form").serialize();
        $.ajax({
            url:admin_url+'statistics/find_match',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){ 
                if(status.msg=='success') { 
                    $(".search_result_body").html(status.response);
                    $(".search_result_div").css("display","");
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



