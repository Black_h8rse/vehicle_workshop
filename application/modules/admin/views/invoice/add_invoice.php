<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>

    <link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/responsive.bootstrap4.min.css" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
        <?php $this->load->view('common/admin_sidebar'); ?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <?php $this->load->view('common/admin_logoutbar'); ?>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Invoice</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>invoice">invoices</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Add invoice</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <input type="hidden" name="count" value="<?php echo $meta_count;?>">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <form method="post" id="add-invoice" enctype="multipart/form-data">
                                    <div class="clients-list">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-quote-left"></i> Customer Detail</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-1" class="tab-pane active">
                                                <div class="full-height-scroll">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox">
                                                                <div class="ibox-content template-option">
                                                                    
                                                                    <div class="add-opt-template">
                                                                        <div class="row">
                                                                            <div class="col-md-12 banner-switcher">
                                                                            <label class="col-sm-4 col-form-label">Customer Type:</label>
                                                                                <div class="i-checks area-status">
                                                                                    <label class="col-md-3"> 
                                                                                        <input type="radio" checked="" value="1" name="customer_type"> <i></i> Regular Customer 
                                                                                    </label>
                                                                                    <label class="col-md-3"> 
                                                                                        <input type="radio" value="0" name="customer_type"> <i></i> New Customer 
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <div class="row regular_customer">
                                                                            <input type="hidden" name="new_customer" id="old_customer" value="1" class="form-control" >
                                                                            <div class="form-group col-md-6">
                                                                                <label>Vehicle:</label>
                                                                                <select class="form-control select2" name="reg_id">
                                                                                   <option>Search by registration number</option> 
                                                                                    <?php if(is_array($vehicles)) { foreach ($vehicles as $key => $value) { ?>
                                                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['reg_no']; ?></option> 
                                                                                    <?php } } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row new_customer" style="display: none;">
                                                                            <div class="row col-md-12">
                                                                                <input type="hidden" name="new_customer" id="new_customer" value="0" class="form-control" >
                                                                                <div class="form-group col-md-4">
                                                                                    <label>Name:</label>
                                                                                    <input type="text" name="name" id="name" class="form-control">
                                                                                </div>
                                                                                <div class="form-group col-md-4">
                                                                                    <label>Phone Number:</label>
                                                                                    <input type="number" name="phone" id="phone" class="form-control">
                                                                                </div>
                                                                                <div class="form-group col-md-4">
                                                                                    <label>Vehicle registration number:</label>
                                                                                    <input type="text" name="v_reg_no" id="v_reg_no" class="form-control">

                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-md-12">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Address:</label>
                                                                                    <input type="text" name="address" id="address" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <button class="btn btn-primary btn-sm pull-right mb-3 add-option">Add Option <i class="fa fa-plus"></i></button>

                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-sm-12"> 
                                                    Total amount:  
                                                    <span style="margin: 10px;" class="label label-warning total_price">  0  </span>
                                                    <input type="hidden" name="total_price_value" id="total_price_value">
                                                    <input type="hidden" name="due_price_value" id="due_price_value">
                                                    <input style="margin: 15px;" type="number" name="paid_amount" id="paid_amount" class="form-control" placeholder="Please enter paid amount">
                                                    <div class="due_div" style="display: none;">
                                                    Due amount:  
                                                    <span style="margin: 10px;" class="label label-warning due_price">  0  </span>
                                                    </div>
                                                    <hr>
                                               <div class="text-right">
                                                    <button class="btn btn-primary btn-sm save-product" type="submit">Save vehicle detail</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view('common/admin_footer'); ?>

        </div>

    </div>
    <?php $this->load->view('common/admin_scripts'); ?>

</body>
</html>

<script type="text/javascript">




    $(document).ready(function () {
        $('input:radio[name="customer_type"]').change( function(){
            console.log($(this).val());
            if ($(this).is(':checked')) {
                console.log($(this).val());
            }
        });
        $('.select2').select2();
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.i-checks').on('ifChecked', function(event){
            console.log(event.target.name);

            if (event.target.name == 'customer_type'){
                if (event.target.value == 1){
                    $('.regular_customer').css('display', '');
                    $('.new_customer').css('display', 'none');
                    $('#old_customer').val('1');
                    $('#new_customer').val('0');
                } else if (event.target.value == 0){
                    $('.regular_customer').css('display', 'none');
                    $('.new_customer').css('display', '');
                    $('#old_customer').val('0');
                    $('#new_customer').val('1');
                }
            }
        });

        var counter = <?php echo $meta_count;?>;
        $('.add-option').on('click', function( e ) {
            e.preventDefault();
            var new_option = '<div class="ibox-content option-row-'+counter+'">\n'+
            '<div class="form-group row">\n'+
                '<label class="col-sm-1 col-form-label">Work:</label>\n'+
                '<div class="col-sm-3">\n'+
                    '<input type="text" class="form-control" name="meta['+counter+'][key]" required="true">\n'+
                '</div>\n'+
                '<label class="col-sm-2 col-form-label pull-right">Amount:</label>\n'+
                '<div class="col-sm-4 amount_enter">\n'+
                    '<input type="number" class="form-control amount_enter" name="meta['+counter+'][value]" required="true">\n'+
                '</div>\n'+
                '<button class="btn btn-danger btn-sm mb-1 remove-option" data-val="'+counter+'"><i class="fa fa-times"></i> Remove Option</button>\n'+
            '</div>\n';
            $('.template-option').append(new_option);
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            counter++;

        });
    });
    $(document).on('blur','.amount_enter', function( e ){
        var sum = 0;
        $('.amount_enter').each(function() {
            sum += Number($(this).val());
        });
        $(".total_price").text(sum);
        $("#total_price_value").val(sum);
    });
    $(document).on('blur','#paid_amount', function( e ){
        var total_amount    = $("#total_price_value").val();
        var paid_amount     = $("#paid_amount").val();
        var due_amount      = total_amount - paid_amount;
        $(".due_price").text(due_amount);
        $(".due_div").css('display', '');
        $("#due_price_value").val(due_amount);
    });

    $(document).on('submit','#add-invoice', function( e ){
        e.preventDefault();
        console.log("yes submited")
        var value = new FormData( $("#add-invoice")[0] );
        $.ajax({
            url:admin_url+'invoice/save_invoice',
            type:'post',
            data:value,
            dataType:'json',
            processData: false,
            contentType: false,
            success:function(status){
                if(status.msg=='success'){
                    toastr.success(status.response, 'Success');
                    setTimeout(function () {
                        $(location).attr('href', admin_url+"invoice");
                    }, 2000);
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response, 'Error');
                }
            }
        });
    });
    $(document).on('click','.remove-option', function( e )
    {
        e.preventDefault();
        var _this = $(this);
        var value = _this.attr('data-val');
        $('.option-row-'+value).remove();
    });
</script>
