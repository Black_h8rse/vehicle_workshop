<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Invoice</h2>
                <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Invoice</strong>
                        </li>
                    </ol>
            </div>
            <div class="col-lg-2">

                <a href="<?php echo admin_url(); ?>invoice/add_invoice" class="btn btn-primary pull-right t_m_25">
                    <i class='fa fa-plus'></i> Add invoice
                </a>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Invoice list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>User Name</th>
                                        <th>Invoice Number</th>
                                        <th>Total amount</th>
                                        <th>Paid amount</th>
                                        <th>Due amount</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($invoices as $invoice){ ?>
                                        <tr id="<?php echo "row_".$i; ?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $invoice['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $invoice['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if( $invoice['user_id'] != 'NEW' ) {  ?>
                                                <td><?php echo get_user_name( $invoice['user_id'] ); ?></td>
                                            <?php } else { ?>
                                                <td><?php echo 'Random Visitor'; ?></td>
                                            <?php } ?>
                                            <td><?php echo $invoice['rendom_no']; ?></td>
                                            <td><?php echo $invoice['total_price']; ?></td>
                                            <td><?php echo $invoice['paid_price']; ?></td>
                                            <td><?php echo $invoice['due_price']; ?></td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($invoice['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $invoice['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                                <!-- <button class="btn btn-primary btn-circle edit-btn" data-id="<?php echo $invoice['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button> -->
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>


<script type="text/javascript">
    $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'invoice/delete_invoice',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"invoice");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".edit-btn" , function() {

        var user_id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>user/edit_user',
            type: 'POST',
            data: { user_id : user_id},
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_user_body').html(status.response);
                    $('#add_user_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



