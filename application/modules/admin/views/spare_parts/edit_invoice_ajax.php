<div class="modal-content animated flipInY">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h5 class="modal-title">Edit User</h5>
    </div>
    <form method="post" id="edit_user_form">
        <div class="modal-body">
            <div class="row">
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user['id']; ?>" class="form-control" required="true">
                <div class="form-group col-md-6">
                    <label>First Name</label>
                    <input type="text" name="f_name" id="f_name" value="<?php echo $user['f_name']; ?>" class="form-control" required="true">
                </div>
                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" name="l_name" id="l_name" value="<?php echo $user['l_name']; ?>" class="form-control" required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Email</label>
                    <input type="text" name="email" id="email" value="<?php echo $user['email']; ?>" class="form-control" disabled>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Phone Number</label> 
                    <input type="number" name="phone" id="phone" value="<?php echo $user['phone']; ?>" class="form-control" required="true">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="button" class="ladda-button btn btn-primary" id="update_user" data-style="expand-right">Update</button>
        </div>
    </form>
</div>
<script>
    $(document).on("click" , "#update_user" , function() {

        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#edit_user_form").serialize();
        $.ajax({
            url:'<?php echo admin_url(); ?>user/update',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#edit_user_form')[0].reset();
                    toastr.success(status.response);
                    $('#user_edit_modal').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
</script>