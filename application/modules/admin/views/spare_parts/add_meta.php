<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>

    <link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/responsive.bootstrap4.min.css" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
        <?php $this->load->view('common/admin_sidebar'); ?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <?php $this->load->view('common/admin_logoutbar'); ?>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Products</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>vehicle">Vehicles</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Add Meta</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <input type="hidden" name="count" value="<?php echo $meta_count;?>">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <?php $vehicle_detail = get_data('','vehicles',array('id'=>$veh_id)  ); ?>
                                <h5>Vehicle Detail <span class="label label-danger"> <?php echo  get_user_name( $vehicle_detail[0]['user_id'] ); ?> </span>  <span class="label label-success"> <?php echo $vehicle_detail[0]['company_name']; ?> </span> <span class="label label-primary"> <?php echo $vehicle_detail[0]['reg_no']; ?> </span> </h5>
                            </div>
                            <div class="ibox-content">
                                <form method="post" id="add-product" enctype="multipart/form-data">
                                    <div class="clients-list">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-quote-left"></i> Option Templates</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-1" class="tab-pane active">
                                                <div class="full-height-scroll">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox">
                                                                <div class="ibox-content template-option">
                                                                    <div class="add-opt-template">
                                                                        <input type="hidden" class="form-control veh_id" name="veh_id" value="<?php echo $veh_id;?>">
                                                                        <button class="btn btn-primary btn-sm pull-right mb-3 add-option">Add Option <i class="fa fa-plus"></i></button>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <?php if(is_array($vehicle_meta)) { foreach ($vehicle_meta as $key => $value) { ?>
                                                                        <div class="ibox-content option-row-'+counter+'">
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-1 col-form-label">Meta Key</label>
                                                                                <div class="col-sm-3">
                                                                                    <input type="text" class="form-control" name="meta[<?php echo $key;?>][key]" value="<?php echo $value['meta_key'];?>" required="true">
                                                                                </div>
                                                                                <label class="col-sm-2 col-form-label pull-right">Meta Value</label>
                                                                                <div class="col-sm-4">
                                                                                <input type="text" class="form-control" name="meta[<?php echo $key;?>][value]" value="<?php echo $value['meta_value'];?>" required="true">
                                                                                </div>
                                                                                <button class="btn btn-danger btn-sm mb-1 remove-option" data-val="<?php echo $key;?>"><i class="fa fa-times"></i> Remove Option</button>
                                                                            </div>
                                                                        </div>
                                                                    <?php } } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-sm-12 text-right">
                                                <button class="btn btn-primary btn-sm save-product" type="submit">Save vehicle detail</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view('common/admin_footer'); ?>

        </div>

    </div>
    <?php $this->load->view('common/admin_scripts'); ?>

    <!-- data tables  -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>

<script>

    $(document).ready(function () {
    var counter = <?php echo $meta_count;?>;
    $('.add-option').on('click', function( e )
    {
        e.preventDefault();
        var new_option = '<div class="ibox-content option-row-'+counter+'">\n'+
        '<div class="form-group row">\n'+
            '<label class="col-sm-1 col-form-label">Meta Key</label>\n'+
            '<div class="col-sm-3">\n'+
                '<input type="text" class="form-control" name="meta['+counter+'][key]" required="true">\n'+
            '</div>\n'+
            '<label class="col-sm-2 col-form-label pull-right">Meta Value</label>\n'+
            '<div class="col-sm-4">\n'+
                '<input type="text" class="form-control" name="meta['+counter+'][value]" required="true">\n'+
            '</div>\n'+
            '<button class="btn btn-danger btn-sm mb-1 remove-option" data-val="'+counter+'"><i class="fa fa-times"></i> Remove Option</button>\n'+
        '</div>\n';
        $('.template-option').append(new_option);
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
        counter++;

    });
});

    $('#add-product').submit(function (e) {
        e.preventDefault();
        var value = new FormData( $("#add-product")[0] );
        $.ajax({
            url:admin_url+'vehicle/save_meta',
            type:'post',
            data:value,
            dataType:'json',
            processData: false,
            contentType: false,
            success:function(status){
                if(status.msg=='success'){
                    toastr.success(status.response, 'Success');
                    setTimeout(function () {
                        $(location).attr('href', admin_url+"vehicle");
                    }, 2000);
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response, 'Error');
                }
            }
        });
    });
$(document).on('click','.remove-option', function( e )
{
    e.preventDefault();
    var _this = $(this);
    var value = _this.attr('data-val');
    $('.option-row-'+value).remove();
});


</script>
