<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-5">
                <h2>Products</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Products</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-7">
                
                <button type="button" class="btn btn-danger pull-right t_m_25" data-toggle="modal" data-target="#sell_product_modal">
                    <i class='fa fa-minus'></i> Sell Product
                </button>

                <button type="button" style="margin-right: 10px;" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_category_modal">
                    <i class='fa fa-plus'></i> Add Product
                </button>
                
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Product list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>Category Name</th>
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($sub_parts as $spare_part){ ?>
                                        <tr id="<?php echo "row_".$i; ?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $spare_part['created_by'] != '1' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $spare_part['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td> <span class="label label-success"><?php print_r( get_name( 'material_cat','id',$spare_part['material_id'],'name' ) ); ?></span>  </td>
                                            <td><?php echo $spare_part['name']; ?></td>
                                            <td>
                                                <?php if($spare_part['quantity'] < '20') { ?>
                                                    <span class="label label-danger">
                                                        <?php echo $spare_part['quantity']; ?>
                                                    </span>
                                                <?php } else { ?>
                                                    <span class="label label-warning">
                                                        <?php echo $spare_part['quantity']; ?>  
                                                    </span>
                                                <?php } ?>  
                                            </td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($spare_part['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $spare_part['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                                <!-- <button class="btn btn-primary btn-circle edit-btn" data-id="<?php echo $spare_part['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button> -->
                                                <button class="btn btn-success btn-circle add_product_quantity_model" data-id="<?php echo $spare_part['id']; ?>" data-name="<?php echo $spare_part['name']; ?>" data-old-qty="<?php echo $spare_part['quantity']; ?>" type="button" data-toggle="modal" data-target="#add_quantity_modal" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_quantity_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_quantity_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add quantity to <span id="pro_add_qty_name" style="color: red;"></span></h5>
                    </div>
                    <form method="post" id="add_quantity_form">
                        <div class="modal-body">
                            
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quantity</label>
                                    <input type="number" name="add_quantity" id="add_quantity" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Price <span class="add_per_piece_price_message" style="color: red;"></span></label>
                                    <input type="number" name="add_price" id="add_price" class="form-control" required="true">
                                    <input type="hidden" name="add_per_piece_price" id="add_per_piece_price" class="form-control" required="true">
                                </div>
                            </div>
                            <input type="hidden" name="pro_id_add_quantity" id="pro_id_add_quantity" value="">
                            <input type="hidden" name="pro_id_old_quantity" id="pro_id_old_quantity" value="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_quantity" data-style="expand-right">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_category_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_category_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Product</h5>
                    </div>
                    <form method="post" id="add_sub_category_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Select Category</label>
                                    <select class="form-control m-b" name="cat_id" id="cat_id" required="true">
                                        <option value="" selected>Select Category</option>
                                        <?php 
                                        if( get_session('admin_id') != '1') {
                                            foreach ( get_data( '','material_cat',array( 'created_by'=> get_session('admin_id') ) ) as $cat) { 
                                                ?>
                                            <option value="<?php echo @$cat['id']; ?>"><?php echo  @$cat['name']; ?></option>
                                        <?php } 
                                        } else {
                                            foreach (get_data('','material_cat') as $cat) {
                                            ?>
                                            <option value="<?php echo @$cat['id']; ?>"><?php echo @$cat['name']; ?></option>
                                        <?php } 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quantity</label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Buy Price  <span class="buy_price_message" style="color: red;"></span></label>
                                    <input type="test" name="buy_price" id="buy_price" class="form-control" required="true">
                                    <input type="hidden" name="price_per_piece" id="price_per_piece" class="form-control" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_sub_category" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="sell_product_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="sell_product_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Sell Product</h5>
                    </div>
                    <form method="post" id="sell_product_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Select Category</label>
                                    <select class="form-control m-b" name="cat_id" id="cat_id" required="true">
                                        <option value="" selected>Select Category</option>
                                        <?php 
                                        if( get_session('admin_id') != '1') {
                                            foreach ( get_data( '','material_cat',array( 'created_by'=> get_session('admin_id') ) ) as $cat) { 
                                                ?>
                                            <option value="<?php echo @$cat['id']; ?>"><?php echo  @$cat['name']; ?></option>
                                        <?php } 
                                        } else {
                                            foreach (get_data('','material_cat') as $cat) {
                                            ?>
                                            <option value="<?php echo @$cat['id']; ?>"><?php echo @$cat['name']; ?></option>
                                        <?php } 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" id="products_div" style="display: none;">
                                <div class="form-group col-md-12" id="products_sub_div">

                                </div>
                            </div>
                            <div class="row" id="products_quantity_div" style="display: none;">
                                <div class="form-group col-md-12">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity_to_sell" id="quantity_to_sell" value="" class="form-control" required="true">
                                </div>
                            </div>
                            <input type="hidden" name="quantity_to_sell_max" id="quantity_to_sell_max" value="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_sell_product" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>


<script type="text/javascript">
    $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    $(document).on('click', '.add_product_quantity_model', function (event) {
        $("#pro_id_add_quantity").val($(this).attr("data-id"));
        $("#pro_id_old_quantity").val($(this).attr("data-old-qty"));
        $("#pro_add_qty_name").text($(this).attr("data-name"));
        
    });
    $(document).on('blur','#add_price', function( e ){
        var per_peice = $(this).val()/$("#add_quantity").val();
        $(".add_per_piece_price_message").text("Price per piece = "+per_peice);
        $("#add_per_piece_price").val(per_peice);
    });

    $(document).on('blur','#buy_price', function( e ){
        var per_peice = $(this).val()/$("#quantity").val();
        $(".buy_price_message").text("Price per piece = "+per_peice);
        $("#price_per_piece").val(per_peice);
    });

    $(document).on("change" , "#cat_id" , function() {

        var id = $(this).val();
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/view_product_list',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#products_sub_div').html(status.response);
                    $('#products_div').css("display", "");
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });

    $(document).on("change" , "#pro_id" , function() {

        var id = $(this).val();
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/view_product_price_input',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){

                    $("#quantity_to_sell").attr({
                       "max" : status.response
                    });
                    $('#products_quantity_div').css("display", "");
                    $('#quantity_to_sell_max').val(status.response);
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });


    $(document).on('click', '.delete-btn', function (event) {

        var cat_id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this category!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'spare_parts/delete_sub_category',
                        type:'post',
                        data:{ cat_id : cat_id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"spare_parts/sub_parts");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".edit-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/edit_sub_category',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_category_body').html(status.response);
                    $('#add_category_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });        
    $(document).on("click" , ".view-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/view_category',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_spare_parts_body').html(status.response);
                    $('#add_category_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_sub_category" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_sub_category_form").serialize();
        $.ajax({
            url:admin_url+'spare_parts/save_sub_category',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_sub_category_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_sub_category_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });    
    $(document).on("click" , "#submit_quantity" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_quantity_form").serialize();
        $.ajax({
            url:admin_url+'spare_parts/submit_quantity',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_quantity_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_quantity_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_sell_product" , function() {
        // var btn = $(this).ladda();
        // btn.ladda('start');
        var formData = $("#sell_product_form").serialize();
        $.ajax({
            url:admin_url+'spare_parts/sell_one_product',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                // btn.ladda('stop');
                if(status.msg=='success') {
                    $('#sell_product_form')[0].reset();
                    toastr.success(status.response);
                    $('#sell_product_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



