<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>

    <link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin_assets/css/datatable/responsive.bootstrap4.min.css" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
        <?php $this->load->view('common/admin_sidebar'); ?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <?php $this->load->view('common/admin_logoutbar'); ?>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Invoice</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo admin_url(); ?>spare_parts\invoice">invoices</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Add invoice</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <input type="hidden" name="count" value="<?php echo $meta_count;?>">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <form method="post" id="add-invoice" enctype="multipart/form-data">
                                    <div class="clients-list">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-quote-left"></i> Customer Detail</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-1" class="tab-pane active">
                                                <div class="full-height-scroll">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox">
                                                                <div class="ibox-content template-option">
                                                                    <input type="hidden" name="new_customer" id="new_customer" value="1" class="form-control" >
                                                                    <div class="add-opt-template">
                                                                        <div class="row">
                                                                            <div class="row col-md-12">
                                                                                <div class="form-group col-md-6">
                                                                                    <label>Name:</label>
                                                                                    <input type="text" name="name" id="name" class="form-control">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label>Phone Number:</label>
                                                                                    <input type="number" name="phone" id="phone" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-md-12">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Address:</label>
                                                                                    <input type="text" name="address" id="address" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <button class="btn btn-primary btn-sm pull-right mb-3 add-option">Add Option  <i class="fa fa-plus"></i></button>

                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-sm-12"> 
                                                    Total amount:  
                                                    <span style="margin: 10px;" class="label label-warning total_price">  0  </span>
                                                    <input type="hidden" name="total_price_value" id="total_price_value">
                                                    <input type="hidden" name="due_price_value" id="due_price_value">
                                                    <input style="margin: 15px;" type="number" name="paid_amount" id="paid_amount" class="form-control" placeholder="Please enter paid amount">
                                                    <div class="due_div" style="display: none;">
                                                    Due amount:  
                                                    <span style="margin: 10px;" class="label label-warning due_price">  0  </span>
                                                    </div>
                                                    <hr>
                                               <div class="text-right">
                                                    <button class="btn btn-primary btn-sm save-product" type="submit">Save detail</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view('common/admin_footer'); ?>
        </div>
    </div>
    <?php $this->load->view('common/admin_scripts'); ?>
</body>
</html>

<script type="text/javascript">

    $(document).ready(function () {
        $('input:radio[name="customer_type"]').change( function(){
            console.log($(this).val());
            if ($(this).is(':checked')) {
                console.log( $(this).val() );
            }
        });
        var counter = <?php echo $meta_count;?>;
        $(document).on('click','.add-option', function( e ) {
            e.preventDefault();
            var new_option = '<div class="ibox-content option-row-'+counter+'">\n'+
            '<div class="form-group row">\n'+
                '<label class="col-sm-1 col-form-label">Part:</label>\n'+
                '<select class="col-sm-3" name="meta['+counter+'][key]" id="pro_id" required="true">\n'+
                '<div class="col-sm-3">\n'+
                '<option value="" selected>Select Sparepart</option>\n'+
                    <?php foreach ($spare_parts as $part) { ?>
                         '<option value="<?php echo $part['id']; ?>" data-max="<?php echo $part['quantity']; ?>" data-class="'+counter+'"><?php echo  @$part['name']; ?></option>\n'+
                    <?php } ?>
                '</select>\n'+
                '<label class="col-sm-2 col-form-label pull-right">Quantity:<span style="color: red;" class="select_max_label_'+counter+'"></span></label>\n'+
                '<div class="col-sm-2">\n'+
                    '<input type="number" class="form-control quantity_enter_'+counter+' quantity_enter" name="meta['+counter+'][quantity_enter]" data-id="'+counter+'" required="true">\n'+
                    '<input type="hidden" class="form-control select_max_'+counter+'" name="meta['+counter+'][quantity_enter_max]">\n'+
                '</div>\n'+
                '<label class="col-sm-1 col-form-label pull-right">Price:<span style="color: red;" class="select_max_price_'+counter+'"></span></label>\n'+
                '<div class="col-sm-2">\n'+
                    '<input type="number" class="form-control amount_enter_'+counter+' amount_enter" name="meta['+counter+'][amount_enter]" required="true">\n'+
                '</div>\n'+
                '<button class="btn btn-danger btn-sm mb-1 remove-option" data-val="'+counter+'"><i class="fa fa-times"></i></button>\n'+
                '<button type="button" class="btn btn-success btn-sm mb-1 add-option-custome" style="margin-left: 10px;"><i class="fa fa-plus"></i></button>\n'+
            '</div>\n';
            $('.template-option').append(new_option);
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            counter++;
        });
        $(document).on('click','.add-option-custome', function( e ) {
            console.log("cliked_custome");
            e.preventDefault();
            var new_option = '<div class="ibox-content option-row-'+counter+'">\n'+
                '<div class="form-group row">\n'+
                    '<label class="col-sm-1 col-form-label">Work:</label>\n'+
                    '<div class="col-sm-3">\n'+
                        '<input type="text" class="form-control" name="meta['+counter+'][key]" required="true">\n'+
                    '</div>\n'+
                    '<label class="col-sm-2 col-form-label pull-right">Amount:</label>\n'+
                    '<div class="col-sm-4">\n'+
                        '<input type="number" class="form-control amount_enter" name="meta['+counter+'][value]" required="true">\n'+
                    '</div>\n'+
                    '<button class="btn btn-danger btn-sm mb-1 remove-option" data-val="'+counter+'"><i class="fa fa-times"></i></button>\n'+
                    '<button type="button" class="btn btn-success btn-sm mb-1 add-option" style="margin-left: 10px;"><i class="fa fa-plus"></i></button>\n'+
                '</div>\n';
            $('.template-option').append(new_option);
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            counter++;
        });
    });
    $(document).on('blur','.amount_enter', function( e ){
        var sum     = 0;
        $('.amount_enter').each(function() {  sum += Number($(this).val()); });
        $(".total_price").text(sum);
        $("#total_price_value").val(sum);
    });
    $(document).on('blur','.quantity_enter', function( e ){
        var enter_quantity  = parseInt( $(this).val() );
        var row_id          = $(this).attr("data-id");
        var max_quantity    = parseInt( $(".select_max_"+row_id).val() );
        if( max_quantity < enter_quantity ) {
            $(this).val("");
            toastr.error("please enter value less them max limit", 'Error');
        }
    });
    $(document).on('blur','#paid_amount', function( e ){
        var total_amount    = $("#total_price_value").val();
        var paid_amount     = $("#paid_amount").val();
        var due_amount      = total_amount - paid_amount;
        $(".due_price").text(due_amount);
        $(".due_div").css('display', '');
        $("#due_price_value").val(due_amount);
    });
    $(document).on('submit','#add-invoice', function( e ){
        e.preventDefault();
        console.log("yes submited");
        var value = new FormData( $("#add-invoice")[0] );
        $.ajax({
            url:admin_url+'spare_parts/save_invoice',
            type:'post',
            data:value,
            dataType:'json',
            processData: false,
            contentType: false,
            success:function(status){
                if(status.msg=='success'){
                    toastr.success(status.response, 'Success');
                    setTimeout(function () {
                        $(location).attr('href', admin_url+"spare_parts/invoice");
                    }, 2000);
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response, 'Error');
                }
            }
        });
    });
    $(document).on('click','.remove-option', function( e ) {
        e.preventDefault();
        var _this = $(this);
        var value = _this.attr('data-val');
        $('.option-row-'+value).remove();
    });
    $(document).on("change" , "#pro_id" , function() {
        var max             = $('option:selected', this).attr('data-max');
        var class_value     = $('option:selected', this).attr('data-class');
        $('.select_input_'+class_value).attr({ "max" : max,"min" : 1 });
        $('.select_max_label_'+class_value).text(" Max "+max);
        $('.select_max_'+class_value).val(max);
    });
</script>
