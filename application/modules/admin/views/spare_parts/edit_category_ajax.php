<div class="modal-content animated flipInY">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h5 class="modal-title">Edit Category</h5>
    </div>
    <form method="post" id="update_cat_form">
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Name</label>
                    <input type="text" name="name" id="name" value="<?php echo $spare_parts['name'] ?>" class="form-control" disabled>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Description</label>
                    <input type="text" name="description" id="description" value="<?php echo $spare_parts['description'] ?>" class="form-control" required="true">
                </div>
            </div>
            <input type="hidden" name="cat_id" value="<?php echo $id?>">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="button" class="ladda-button btn btn-primary" id="update_category" data-style="expand-right">Update</button>
        </div>
    </form>
</div>
<script>
    $(document).on("click" , "#update_category" , function() {

        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#update_cat_form").serialize();
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/update_cat',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#update_cat_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_vehicle_modal').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
</script>