<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Categories</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Categories</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_category_modal">
                    <i class='fa fa-plus'></i> Add category
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Categories list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($spare_parts as $spare_part){ ?>
                                        <tr id="<?php echo "row_".$i; ?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $spare_part['created_by'] != '1' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $spare_part['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td><?php echo $spare_part['name']; ?></td>
                                            <td><?php echo $spare_part['description']; ?></td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($spare_part['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $spare_part['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                                <button class="btn btn-primary btn-circle edit-btn" data-id="<?php echo $spare_part['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                               <!--  <button class="btn btn-success btn-circle view-btn" data-id="<?php echo $spare_part['id']; ?>" type="button" data-toggle="modal" data-target="#add_sub_cat_modal" data-placement="top" title="Add Category Product"><i class="fa fa-list-alt"></i></button> -->
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_category_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_category_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Category</h5>
                    </div>
                    <form method="post" id="add_category_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Description</label>
                                    <input type="text" name="description" id="description" class="form-control" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_category" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>




        <div class="modal inmodal" id="add_sub_cat_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_spare_parts_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add Category Product</h5>
                    </div>
                    <form method="post" id="add_sub_category_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Name</label>
                                    <input type="test" name="name" id="name" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Quatity</label>
                                    <input type="test" name="quantity" id="quantity" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Buy Price</label>
                                    <input type="test" name="buy_price" id="buy_price" class="form-control" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_sub_category" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>


<script type="text/javascript">
    $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });

    $(document).on('click', '.delete-btn', function (event) {

        var cat_id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this category!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'spare_parts/delete_category',
                        type:'post',
                        data:{ cat_id : cat_id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"spare_parts");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".edit-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/edit_category',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_category_body').html(status.response);
                    $('#add_category_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });        
    $(document).on("click" , ".view-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>spare_parts/view_category',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_spare_parts_body').html(status.response);
                    $('#add_category_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_category" , function() {
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_category_form").serialize();
        $.ajax({
            url:admin_url+'spare_parts/save_category',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_category_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_category_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



