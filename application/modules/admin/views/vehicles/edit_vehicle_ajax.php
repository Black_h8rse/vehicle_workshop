<div class="modal-content animated flipInY">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h5 class="modal-title">Update Vehicle</h5>
    </div>
    <form method="post" id="edit_vehicles_form">
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Select User</label>
                    <select class="form-control m-b" name="user_id" id="user_id" required="true">
                        <option value="" selected>Select User</option>
                        <?php 
                        if( get_session('admin_id') != '1') {
                            foreach ( get_data( '','users',array( 'created_by'=> get_session('admin_id') ) ) as $user) { 
                                ?>
                            <option value="<?php echo @$user['id']; ?>" <?= $user['id'] == $vehicles['user_id'] ? ' selected="selected"' : '';?>><?php echo  @$user['f_name'].' '.@$user['l_name']; ?></option>
                        <?php } 
                        } else {
                            foreach (get_data('','users') as $user) {
                            ?>
                            <option value="<?php echo @$user['id']; ?>" <?= $user['id'] == $vehicles['user_id'] ? ' selected="selected"' : '';?> ><?php echo @$user['f_name'].' '.@$user['l_name']; ?></option>
                        <?php } 
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Registration Number</label>
                    <input type="test" name="reg_no" id="reg_no" value="<?php echo $vehicles['reg_no']; ?>" class="form-control" required="true">
                </div>
            </div>  
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Brand:</label>
                    <input type="test" name="company_name" id="company_name" value="<?php echo $vehicles['company_name']; ?>" class="form-control" required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Model:</label>
                    <input type="test" name="model" id="model" value="<?php echo $vehicles['model']; ?>" class="form-control" required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Year Of Make:</label>
                    <input type="number" name="year_of_make" id="year_of_make" value="<?php echo $vehicles['year_of_make']; ?>" class="form-control" required="true">
                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo $vehicles['id']; ?>">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="button" class="ladda-button btn btn-primary" id="update_vehicle" data-style="expand-right">Update</button>
        </div>
    </form>
</div>
<script>
    $(document).on("click" , "#update_vehicle" , function() {

        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#edit_vehicles_form").serialize();
        $.ajax({
            url:'<?php echo admin_url(); ?>vehicle/update',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#edit_vehicles_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_vehicle_modal').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
</script>