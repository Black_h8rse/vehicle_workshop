<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Vehicles</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Vehicles</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_vehicle_modal">
                    <i class='fa fa-plus'></i> Add Vehicle
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Vehicle list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>Owner Name</th>
                                        <th>Brand</th>
                                        <th>Model</th>
                                        <th>Year of make</th>
                                        <th>Registration Number</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($vehicles as $vehicle){ ?>
                                        <tr id="<?php echo "row_".$i; ?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $vehicle['created_by'] != '1' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $vehicle['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td><?php echo get_user_name( $vehicle['user_id'] ); ?></td>
                                            <td><?php echo $vehicle['company_name']; ?></td>
                                            <td><?php echo $vehicle['model']; ?></td>
                                            <td><?php echo $vehicle['year_of_make']; ?></td>
                                            <td><?php echo $vehicle['reg_no']; ?></td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($vehicle['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $vehicle['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                                <button class="btn btn-primary btn-circle edit-btn" data-id="<?php echo $vehicle['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                <a href="<?php echo admin_url(); ?>vehicle/add_meta/<?php echo $vehicle['id']; ?>" class="btn btn-warning btn-circle" data-toggle="tooltip" title="Add meta" data-placement="top"><i class="fa fa-plus"></i></a>
                                                <button class="btn btn-success btn-circle view-btn" data-id="<?php echo $vehicle['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-success btn-circle invoice-btn" data-id="<?php echo $vehicle['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Invoices View"><i class="fa fa-gear"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_vehicle_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_vehicles_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Vehicle</h5>
                    </div>
                    <form method="post" id="add_vehicle_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Select User</label>
                                    <select class="form-control m-b" name="user_id" id="user_id" required="true">
                                        <option value="" selected>Select User</option>
                                        <?php 
                                        if( get_session('admin_id') != '1') {
                                            foreach ( get_data( '','users',array( 'created_by'=> get_session('admin_id') ) ) as $user) { 
                                                ?>
                                            <option value="<?php echo @$user['id']; ?>"><?php echo  @$user['f_name'].' '.@$user['l_name']; ?></option>
                                        <?php } 
                                        } else {
                                            foreach (get_data('','users') as $user) {
                                            ?>
                                            <option value="<?php echo @$user['id']; ?>"><?php echo @$user['f_name'].' '.@$user['l_name']; ?></option>
                                        <?php } 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Registration Number</label>
                                    <input type="test" name="reg_no" id="reg_no" class="form-control" required="true">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Brand:</label>
                                    <input type="test" name="company_name" id="company_name" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Model:</label>
                                    <input type="test" name="model" id="model" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Year Of Make:</label>
                                    <input type="number" name="year_of_make" id="year_of_make" class="form-control" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_vehicle" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="modal inmodal" id="add_ajax_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_ajax_body">
                
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>


<script type="text/javascript">
    $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });

    $(document).on('click', '.delete-btn', function (event) {

        var user_id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this vehicle!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'vehicle/delete_vehicle',
                        type:'post',
                        data:{ user_id : user_id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"vehicle");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".edit-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>vehicle/edit_vehicle',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_ajax_body').html(status.response);
                    $('#add_ajax_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    }); 
    $(document).on("click" , ".invoice-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>vehicle/view_invoices',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_ajax_body').html(status.response);
                    $('#add_ajax_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });       
    $(document).on("click" , ".view-btn" , function() {

        var id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>vehicle/view_vehicle',
            type: 'POST',
            data: { id : id },
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                     $('#add_ajax_body').html(status.response);
                    $('#add_ajax_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_vehicle" , function() {
        console.log("yes submit_vehicle");
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_vehicle_form").serialize();
        $.ajax({
            url:admin_url+'vehicle/save_vehicle',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_vehicle_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_vehicle_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



