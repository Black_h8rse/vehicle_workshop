<style type="text/css">
    body{
    margin-top:20px;
    color: #484b51;
    }
    .text-secondary-d1 {
        color: #728299!important;
    }
    .page-header {
        margin: 0 0 1rem;
        padding-bottom: 1rem;
        padding-top: .5rem;
        border-bottom: 1px dotted #e2e2e2;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -ms-flex-align: center;
        align-items: center;
    }
    .page-title {
        padding: 0;
        margin: 0;
        font-size: 1.75rem;
        font-weight: 300;
    }
    .brc-default-l1 {
        border-color: #dce9f0!important;
    }

    .ml-n1, .mx-n1 {
        margin-left: -.25rem!important;
    }
    .mr-n1, .mx-n1 {
        margin-right: -.25rem!important;
    }
    .mb-4, .my-4 {
        margin-bottom: 1.5rem!important;
    }

    hr {
        margin-top: 1rem;
        margin-bottom: 1rem;
        border: 0;
        border-top: 1px solid rgba(0,0,0,.1);
    }

    .text-grey-m2 {
        color: #888a8d!important;
    }

    .text-success-m2 {
        color: #86bd68!important;
    }

    .font-bolder, .text-600 {
        font-weight: 600!important;
    }

    .text-110 {
        font-size: 110%!important;
    }
    .text-blue {
        color: #478fcc!important;
    }
    .pb-25, .py-25 {
        padding-bottom: .75rem!important;
    }

    .pt-25, .py-25 {
        padding-top: .75rem!important;
    }
    .bgc-default-tp1 {
        background-color: rgba(121,169,197,.92)!important;
    }
    .bgc-default-l4, .bgc-h-default-l4:hover {
        background-color: #f3f8fa!important;
    }
    .page-header .page-tools {
        align-self: flex-end;
    }

    .btn-light {
        color: #757984;
        background-color: #f5f6f9;
        border-color: #dddfe4;
    }
    .w-2 {
        width: 1rem;
    }

    .text-120 {
        font-size: 120%!important;
    }
    .text-primary-m1 {
        color: #4087d4!important;
    }

    .text-danger-m1 {
        color: #dd4949!important;
    }
    .text-blue-m2 {
        color: #68a3d5!important;
    }
    .text-150 {
        font-size: 150%!important;
    }
    .text-60 {
        font-size: 60%!important;
    }
    .text-grey-m1 {
        color: #7b7d81!important;
    }
    .align-bottom {
        vertical-align: bottom!important;
    }
</style>


<div class="modal-content animated flipInY" style="width: 800px;"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button>
        <h5 class="modal-title">View Vehicle Detail</h5>
    </div>
    <div class="page-content container">
        <div class="page-header text-blue-d2">
        </div>
        <div class="container px-0">
            <div class="row mt-4">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <!-- .row -->
                    <hr class="row brc-default-l1 mx-n1 mb-4" />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-grey-m2">
                                <?php if(get_session('admin_id') == '1'){?>
                                    <div class="my-1" style="height: 30px;">
                                        Created By: <span class="label label-danger" style="float: right;">   <?php echo get_user_name( $vehicles['created_by'] ); ?> </span>
                                    </div>
                                <?php } ?>
                                <div class="my-1" style="height: 30px;">
                                    Vehicle owner Name: <span class="label label-success" style="float: right;">   <?php echo get_user_name( $vehicles['user_id'] ); ?> </span>
                                </div>
                                <div class="my-1" style="height: 30px;">
                                    Vehicle company Name:  <span class="label label-warning" style="float: right;"> <?php echo $vehicles['company_name']; ?> </span>
                                </div>
                                <div class="my-1" style="height: 30px;">
                                    Vehicle registration Number:  <span class="label label-primary" style="float: right;">  <?php echo $vehicles['reg_no']; ?> </span>
                                </div>
                                <div class="my-1" style="height: 30px;">
                                    Total Amount:  <span class="label label-info" style="float: right;">  <?php echo $total_price; ?> </span>
                                </div>
                                <div class="my-1" style="height: 30px;">
                                    Total Paid Amount:  <span class="label label-default" style="float: right;">  <?php echo $total_paid_price; ?> </span>
                                </div>
                                <div class="my-1" style="height: 30px;">
                                    Due Amount:  <span class="label label-danger" style="float: right;">  <?php echo $total_due_price; ?> </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4">

                        <div class="row text-600 text-white bgc-default-tp1 py-25">
                            <?php if(get_session('admin_id') == '1'){?>
                                <div class="col-2 col-sm-2">Created By</div>
                            <?php } else { ?>
                                <div class="d-none d-sm-block col-1">#</div>
                            <?php } ?>
                            <div class="d-none d-sm-block col-2 col-sm-2">Invoice Number</div>
                            <div class="d-none d-sm-block col-2 col-sm-2">Amount</div>
                            <div class="d-none d-sm-block col-1 col-sm-1">Pay</div>
                            <div class="d-none d-sm-block col-1 col-sm-1">Due</div>
                            <div class="d-none d-sm-block col-2 col-sm-2">Date</div>
                            <!-- <div class="d-none d-sm-block col-1 col-sm-1">Action</div> -->
                        </div>
                        <div class="text-95 text-secondary-d3">
                            <?php if( $meta_count > 0 ) { foreach ($invoices as $key => $value) { ?>
                                <div class="row mb-2 mb-sm-0 py-25">
                                    <?php if( get_session('admin_id') == '1' ) {  ?>
                                        <?php if( $value['created_by'] != '0' ) {  ?>
                                             <div class="col-2 col-sm-2">
                                                <span class="label label-success"><?php echo get_user_name( $value['created_by'] ); ?></span>
                                            </div>
                                        <?php }else{ ?>
                                            <div class="col-2 col-sm-2">
                                                <span class="label label-warning"><?php echo "Super Admin"; ?></span></div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div class="d-none d-sm-block col-1"><?php echo $key;?></div>
                                    <?php } ?>
                                    <div class="col-2 col-sm-2"><?php echo $value['rendom_no'];?></div>
                                    <div class="col-2 col-sm-2"><?php echo $value['total_price'];?></div>
                                    <div class="col-1 col-sm-1"><?php echo $value['paid_price'];?></div>
                                    <div class="col-1 col-sm-1"><?php echo $value['due_price'];?></div>
                                    <div class="d-none d-sm-block col-2"> 
                                        <?php echo date('F jS, Y - h:i a' ,strtotime($value['created_at'])); ?>  
                                    </div>
                                    <!-- <div class="d-none d-sm-block col-1">
                                        <a href="<?php echo admin_url(); ?>vehicle/add_meta/<?php echo $value['id']; ?>" class="btn btn-warning btn-circle" data-toggle="tooltip" title="Add meta" data-placement="top"><i class="fa fa-plus"></i></a>
                                    </div> -->
                                </div>
                            <?php } } else {?>
                                <div class="row mb-2 mb-sm-0 py-25">
                                    <div class="d-none d-sm-block col-1">1</div>
                                    <div class="col-2 col-sm-2">Empty</div> 
                                    <div class="d-none d-sm-block col-2 col-sm-2">Empty</div>
                                    <div class="d-none d-sm-block col-1 col-sm-1">Empty</div>
                                    <div class="d-none d-sm-block col-1 col-sm-1">Empty</div>
                                    <div class="d-none d-sm-block col-2 col-sm-2">Empty</div>
                                    <div class="d-none d-sm-block col-1 col-sm-1">Empty</div>
                                </div>
                            <?php }?>
                        </div>
                        <div class="row border-b-2 brc-default-l2"></div>
                        <hr /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>