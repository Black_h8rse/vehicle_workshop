<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Mechanic Users</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Mechanic Users</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_user_modal">
                    <i class='fa fa-plus'></i> Add Mechanic User
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Users Activity list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="users-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($users as $user){ ?>
                                        <tr id="<?php echo "row_".$i; ?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $user['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $user['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td><?php echo $user['f_name']; ?></td>
                                            <td><?php echo $user['l_name']; ?></td>
                                            <td><?php echo $user['email']; ?></td>
                                            <td><?php echo $user['phone']; ?></td>
                                            <td>
                                                <?php if($user['user_type'] == 1) { ?>
                                                    <span class="label label-primary">Super Admin</span>
                                                <?php } elseif($user['user_type'] == 2) { ?>
                                                    <span class="label label-danger">Workshop Owner</span>
                                                <?php } elseif($user['user_type'] == 3) { ?>
                                                    <span class="label label-worning">Vehicle Owner</span>
                                                <?php } elseif($user['user_type'] == 4) { ?>
                                                    <span class="label label-success">Spare Part Shop Owner</span>
                                                <?php } elseif($user['user_type'] == 5) { ?>
                                                    <span class="label label-info">Mechanic</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($user['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $user['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button>
                                                <button class="btn btn-primary btn-circle edit-btn" data-id="<?php echo $user['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_user_modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_user_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Mechanic User</h5>
                    </div>
                    <form method="post" id="add_user_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="text" name="f_name" id="f_name" class="form-control" required="true">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" name="l_name" id="l_name" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Email</label>
                                    <input type="text" name="email" id="email" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Phone Number</label>
                                    <input type="number" name="phone" id="phone" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Password</label>
                                    <input type="password" name="password" id="password" class="form-control" required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Confirm Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" required="true">
                                </div>
                            </div>
                            <input type="hidden" name="user_type" id="user_type" value="2">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_user" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script>


</body>
</html>


<script type="text/javascript">
    $('#users-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 7, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $(document).on('click', '.delete-btn', function (event) {

        var user_id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'user/delete_user',
                        type:'post',
                        data:{ user_id : user_id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            setTimeout(function () {
                                $(location).attr('href', admin_url+"user");
                            }, 2000);
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
    $(document).on("click" , ".edit-btn" , function() {

        var user_id = $(this).attr('data-id');
        $.ajax({
            url:'<?php echo admin_url(); ?>user/edit_user',
            type: 'POST',
            data: { user_id : user_id},
            dataType:'json',
            success:function(status){
                if(status.msg=='success'){
                    $('#add_user_body').html(status.response);
                    $('#add_user_modal').modal('show'); 
                }
                else if(status.msg == 'error'){
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_user" , function() {
        console.log("yes submit_user");
        var btn = $(this).ladda();
        btn.ladda('start');
        var formData = $("#add_user_form").serialize();
        $.ajax({
            url:admin_url+'user/save_user',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){
                btn.ladda('stop');
                if(status.msg=='success') {
                    $('#add_user_form')[0].reset();
                    toastr.success(status.response);
                    $('#add_user_form').modal('hide');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



