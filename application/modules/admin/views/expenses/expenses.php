<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('common/admin_header'); ?>
</head>

<body>
<div id="wrapper">
    <?php $this->load->view('common/admin_sidebar'); ?>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <?php $this->load->view('common/admin_logoutbar'); ?>
        </div>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-6">
                <h2>Expenses</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo admin_url(); ?>dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Expenses</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-6"> 
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_expense_type_model"><i class='fa fa-plus'></i> Add expense type</button>
                <button type="button" class="btn btn-primary pull-right t_m_25" data-toggle="modal" data-target="#add_expense_model" style="  margin-right: 5px;"><i class='fa fa-plus'></i> Add expense</button> 
            </div>
            
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Invoice list</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="expense-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <?php if(get_session('admin_id') == '1'){?>
                                            <th>Created By</th>
                                        <?php } ?>
                                        <th>Expense Name</th> 
                                        <th>Price</th>
                                        <th>Detail</th> 
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($expenses as $expense){ ?>
                                        <tr id="<?php echo "row_".$i; ?>">
                                            <td><?php echo $i; ?></td>
                                            <?php if( get_session('admin_id') == '1' ) {  ?>
                                                <?php if( $expense['created_by'] != '0' ) {  ?>
                                                    <td> <span class="label label-success"><?php echo get_user_name( $expense['created_by'] ); ?></span>  </td>
                                                <?php }else{ ?>
                                                    <td> <span class="label label-warning"><?php echo "Super Admin"; ?></span>  </td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td><?php echo get_name('expense_type','id',$expense['expense_type_id'],'1'); ?></td> 
                                            <td><?php echo $expense['meta_value']; ?></td>
                                            <td><?php echo $expense['detail']; ?></td> 
                                            <td><?php echo date('F jS, Y - h:i a' ,strtotime($expense['created_at'])); ?></td>  
                                            <td>
                                                <button class="btn btn-danger btn-circle delete-btn" data-id="<?php echo $expense['id']; ?>" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></button> 
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_expense_type_model" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_expense_type_model_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New Expense Type</h5>
                    </div>
                    <form method="post" id="add_expense_type_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Expense name</label>
                                    <input type="text" name="name" id="name" class="form-control" required="true">
                                </div>
                                
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_expense_type" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="add_expense_model" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" id="add_expense_model_body">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title">Add New User</h5>
                    </div>
                    <form method="post" id="add_expense_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Expense</label>
                                    
                                    <select class="form-control" name="expense_type_id" id="expense_type_id">
                                        <option value="">Please select type</option>
                                        <?php  foreach ( get_table('expense_type') as $value ) { ?> 
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Price</label>
                                    <input type="number" name="price" id="price" class="form-control" required="true">
                                </div> 
                            </div> 
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Detail</label>
                                    <input type="text" name="detail" id="detail" class="form-control" required="true">
                                </div> 
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="ladda-button btn btn-primary" id="submit_expense" data-style="expand-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/admin_footer'); ?>
    </div>
</div>
<?php $this->load->view('common/admin_scripts'); ?>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_assets/js/datatable/responsive.bootstrap4.min.js"></script> 
</body>
</html>


<script type="text/javascript">
    $('#expense-table').dataTable({
        "paging": true,
        "searching": true,
        "responsive": true,
        "order": [[ 0, 'asc' ]],
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 2, "targets": -1 }
        ]
    });
    $(document).on('click', '.delete-btn', function (event) {

        var id = $(this).attr('data-id');
        
        swal({
                title: "Are you sure?",
                text: "You want to delete this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:admin_url+'expenses/delete_expense',
                        type:'post',
                        data:{ id : id },
                        dataType:'json',
                        success:function(status){
                            toastr.success(status.response, 'Success');
                            $("#row_"+id).css("display","none");
                            
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
 
    $(document).on("click" , "#submit_expense_type" , function() {  
        var formData = $("#add_expense_type_form").serialize();
        $.ajax({
            url:admin_url+'expenses/add_expense_type',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){ 
                if(status.msg=='success') {  
                    $('#add_expense_type_form')[0].reset();
                    $('#add_expense_type_model').modal('hide');
                    toastr.success(status.response);
                    setTimeout(function(){ location.reload(); }, 2000); 
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });
    $(document).on("click" , "#submit_expense" , function() {  
        var formData = $("#add_expense_form").serialize();
        $.ajax({
            url:admin_url+'expenses/add_expense',
            type: 'POST',
            data: formData,
            dataType:'json',
            success:function(status){ 
                if(status.msg=='success') {  
                    $('#add_expense_form')[0].reset();
                    $('#add_expense_model').modal('hide');
                    toastr.success(status.response);
                    setTimeout(function(){ location.reload(); }, 2000); 
                } else if(status.msg == 'error') {
                    toastr.error(status.response);
                }
            }
        });
    });

</script>



