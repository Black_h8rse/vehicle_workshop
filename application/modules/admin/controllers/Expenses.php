<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'expenses_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index() { 

        $data['expenses'] = $this->expenses_model->get_expenses();
        $this->load->view('expenses/expenses',$data);

	}
    public function add_expense() { 
      if ($_POST){
            $data   = $this->input->post();             
            $this->form_validation->set_rules('expense_type_id', 'Expense type', 'required');
            $this->form_validation->set_rules('price', 'Price', 'required'); 

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{ 
                $slider_id = $this->expenses_model->add_expense($data);
                $finalResult = array('msg' => 'success', 'response' => "successfully inserted.");
                echo json_encode($finalResult);
                exit;  

            }   
        }else{
            show_admin404();
        }   
    }   
    public function add_expense_type() { 
      if ($_POST){
            $data   = $this->input->post();
            if(!empty($data['name'])){
                $this->expenses_model->add_expense_type($data);
                $finalResult = array('msg' => 'success', 'response' => "successfully inserted");
                echo json_encode($finalResult);
                exit; 
            }  else { 
                $finalResult = array('msg' => 'error', 'response' => "Please enter expense name");
                echo json_encode($finalResult);
                exit; 
            }   
        }else{
            show_admin404();
        }   
    } 
    public function delete_expense() {
        if ($_POST){
            $data = $this->input->post();
            $this->expenses_model->delete_expense($data['id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.");
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    } 
}
