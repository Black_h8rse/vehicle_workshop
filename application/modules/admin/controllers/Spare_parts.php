<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spare_parts extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'spare_parts_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['spare_parts'] = $this->spare_parts_model->get_spare_parts();
        $this->load->view('spare_parts/spare_parts', $data);
	}
    public function invoice()
    {
        $data['invoices'] = $this->spare_parts_model->get_invoices();
        $this->load->view('spare_parts/invoice', $data);
    }
    public function add_invoice()
    {
        $data['users']          = get_table('users');
        if( get_session('admin_id') != '1' ) {
            $data['spare_parts']    = get_data('','material_pro',array('created_by'=>get_session('admin_id')));
        } else {
            $data['spare_parts']    = get_data('','material_pro');
        }
        
        $data['meta_count']     = '0';
        $this->load->view('spare_parts/add_invoice', $data);
    }
    public function sub_parts()
    {
        $data['spare_parts'] = $this->spare_parts_model->get_spare_parts();
        $data['sub_parts'] = $this->spare_parts_model->get_sub_parts();
        $this->load->view('spare_parts/sub_parts', $data);
    }
	public function delete_category() {
        if ($_POST){
            $data = $this->input->post();
            $this->spare_parts_model->delete_category($data['cat_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['cat_id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function delete_sub_category() {
        if ($_POST){
            $data = $this->input->post();
            $this->spare_parts_model->delete_sub_category($data['cat_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['cat_id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_category()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('name', 'Category Name', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{

                if ( empty( get_data('','material_cat',array('name'=>$data['name'],'created_by'=>get_session('admin_id')) ) ) ) {
                    $category_id = $this->spare_parts_model->insert_category($data);
                    $finalResult = array('msg' => 'success', 'response' => "Category successfully inserted.", 'id' => $category_id);
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Name already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function save_invoice()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('paid_amount', 'Paid amount', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $data['rendom_key'] = check_and_generate_key('invoices','rendom_no');
                $invoice_id         = $this->spare_parts_model->insert_invoice($data); 
                $finalResult        = array('msg' => 'success', 'response' => "Invoice successfully created.");
                echo json_encode($finalResult);
                exit; 
            }
        }else{
            show_admin404();
        }
    }
    public function save_sub_category()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('cat_id', 'Category', 'required');
            $this->form_validation->set_rules('name', 'Sub category name', 'required');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('buy_price', 'Buy price', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $sub_category_id = $this->spare_parts_model->insert_sub_category($data);
                $finalResult = array('msg' => 'success', 'response' => "Sub category successfully inserted.", 'id' => $sub_category_id);
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function edit_category()
    {
        if($_POST){
            $data = $_POST;
            $data['spare_parts'] = $this->spare_parts_model->get_category_details($data);
            if(!empty($data['spare_parts'])) {

                $htmlrespon = $this->load->view('spare_parts/edit_category_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['spare_parts']);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function view_category()
    {
        if($_POST){
            $data = $_POST;
            $data['vehicles']       = $this->spare_parts_model->get_vehicle_details($data);
            $data['veh_id']         = $data['id'];
            $data['vehicle_meta']   = $this->spare_parts_model->get_vehicle_meta_details($data);
            $data['meta_count']     = count($data['vehicle_meta']);
            if(!empty($data['vehicles'])) {

                $htmlrespon = $this->load->view('vehicles/view_vehicle_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['vehicles']);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function update_cat()
    {
        if($_POST){
            $data = $_POST;
            $this->form_validation->set_rules('description', 'Description', 'required');

            if ($this->form_validation->run($this) == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_status = $this->spare_parts_model->update_category($data);
                if($user_status > 0){
                    $finalResult = array('msg' => 'success', 'response'=>"Category successfully updated.");
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function add_meta($id = '')
    {
        $data['veh_id']         = $id;
        $data['vehicle_meta']   = $this->spare_parts_model->get_vehicle_meta_details($data);
        $data['meta_count']     = count($data['vehicle_meta']);
        $this->load->view('vehicles/add_meta',$data);
    }
    public function save_meta()
    {
        if ($_POST){
            $data = $this->input->post();
            $pro_info_id    = $this->spare_parts_model->insert_vehicle_meta($data['meta'],$data['veh_id']);
             $finalResult = array('msg' => 'success', 'response' => "Vehicle Information inserted.", 'id' => $data['veh_id']);
            echo json_encode($finalResult);
            exit;
        } else {
            show_admin404();
        }
    }
    public function view_product_list()
    {
        if ($_POST){
            $data       = $this->input->post();
            $products   = $this->spare_parts_model->get_category_products($data);
            $htmlrespon = $this->load->view('spare_parts/pro_list_ajax' , $data,TRUE);
            $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['id']);
            echo json_encode($finalResult);
            exit;
        } else {
            show_admin404();
        }
    }
    public function view_product_price_input()
    {
        if ($_POST){
            $data       = $this->input->post();
            $products   = $this->spare_parts_model->get_category_products($data);
            $data['product_detail'] = get_data('','material_pro',array('id'=>$data['id'],'created_by'=>get_session('admin_id')));

            $finalResult    = array('msg' => 'success', 'response'=>$data['product_detail'][0]['quantity']);
            echo json_encode($finalResult);
            exit;
        } else {
            show_admin404();
        }
    }
    public function submit_quantity()
    {
        if ($_POST){
            $data           = $this->input->post();
            $products       = $this->spare_parts_model->update_product_quantity($data);
            $finalResult    = array('msg' => 'success', 'response'=>'successfully added');
            echo json_encode($finalResult);
            exit;
        } else {
            show_admin404();
        }
    }
    public function sell_one_product()
    {
        if ($_POST){
            $data   = $this->input->post();
            $this->form_validation->set_rules('quantity_to_sell', 'Quantity', 'required|less_than['.$data["quantity_to_sell_max"].']|greater_than[0]');
            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $products    = $this->spare_parts_model->product_sold($data);
                $finalResult = array('msg' => 'success', 'response'=>'Sold');
                echo json_encode($finalResult);
                exit;
            }
        } else {
            show_admin404();
        }
    }
}
