<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'feedback_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['feedbacks'] = $this->feedback_model->get_feedback();
        $this->load->view('feedback', $data);
	}
	public function delete() {
        if ($_POST){
            $data           = $this->input->post();
            $slider_id      = $this->feedback_model->delete($data['id']);
            $finalResult    = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('feedback', 'Feedback', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $feedback_id = $this->feedback_model->insert($data);
                $finalResult = array('msg' => 'success', 'response' => "Feedback successfully inserted.", 'id' => $feedback_id);
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function edit_user()
    {
        if($_POST){
            $data = $_POST;
            $data['user'] = $this->feedback_model->get_user_details($data);
            if(!empty($data['user'])) {

                $htmlrespon = $this->load->view('users/edit_user_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function update()
    {
        if($_POST){
            $data = $_POST;

            $this->form_validation->set_rules('f_name', 'First Name', 'required');
            $this->form_validation->set_rules('l_name', 'Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required');

            if ($this->form_validation->run($this) == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_status = $this->feedback_model->update_user($data);
                if($user_status > 0){
                    $finalResult = array('msg' => 'success', 'response'=>"Feedback successfully updated.");
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
}
