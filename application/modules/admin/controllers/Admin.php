<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'admin_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        // $data['users'] = $this->admin_model->get_users();
        // $this->load->view('dashboard', $data);
        $this->load->view('dashboard');
	}
	public function dashboard()
	{	
		$this->load->view('dashboard');
	}
	public function change_password()
	{
		$this->load->view('change_password');
	}
	public function update_password()
	{
		$data = $_POST;
		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean|callback_check_old_password');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|callback_check_new_password');
		$this->form_validation->set_rules('c_password', 'Confirm Password', 'trim|required|matches[new_password]|xss_clean');
		if ($this->form_validation->run($this) == FALSE)
		{
			$finalResult = array('msg' => 'error', 'response'=>validation_errors());
			echo json_encode($finalResult);
			exit;
		}else{
			$status = $this->admin_model->change_admin_password($data);
			if($status){

				$finalResult = array('msg' => 'success', 'response'=>'Your password successfully changed!');
				echo json_encode($finalResult);
				exit;
				
			}else{
				$finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
				echo json_encode($finalResult);
				exit;
			}
		}
	}
	public function check_old_password()
	{
		$data = $_POST;
		$status = $this->admin_model->check_old_password($data);
		if ($status > 0)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_old_password', 'Old password is wrong.');
			return FALSE;
		}	
	}
	public function get_complain_customer_detail()
	{
		$data = $_POST; 
		$data['complain_detail'] = get_data($data['complain_id'],'vehicle_complains'); 
		if( $data['complain_detail'][0]['status'] == '2' ){ 
			$data['customer_detail'] 			= get_data($data['complain_detail'][0]['complain_by'],'users');
			$data['customer_vehicle_detail'] 	= get_data('','vehicles',array("user_id"=>$data['complain_detail'][0]['complain_by'])); 
			update_complain($data['complain_id']);  
			$htmlrespon = $this->load->view('complain_customer_detail_ajax' , $data,TRUE);
            $finalResult = array('msg' => 'success', 'response'=>$htmlrespon );
            echo json_encode($finalResult);
            exit;  
		} else {
			$finalResult = array("msg" => "error","response" => "Sorry to say! but another mechanic just approve this request");
			echo json_encode($finalResult);
			exit; 
		}
	}
}
