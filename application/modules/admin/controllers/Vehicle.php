<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'vehicle_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['vehicles'] = $this->vehicle_model->get_vehicles();
        $this->load->view('vehicles/vehicles', $data);
	}
	public function delete_vehicle() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->vehicle_model->delete_vehicle($data['user_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['user_id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_vehicle()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('user_id', 'User name', 'required');
            $this->form_validation->set_rules('reg_no', 'Registration number', 'required');
            $this->form_validation->set_rules('company_name', 'Company name', 'required');
            $this->form_validation->set_rules('model', 'Model', 'required');
            $this->form_validation->set_rules('year_of_make', 'Year Of Make', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{

                if ( empty( get_data('','vehicles',array( 'reg_no'=> $data['reg_no'] ) ) ) ) {
                    $vehicles_id = $this->vehicle_model->insert_vehicles($data);
                    $finalResult = array('msg' => 'success', 'response' => "Vehicles successfully inserted.", 'id' => $vehicles_id);
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Registration number already exist');
                    echo json_encode($finalResult);
                    exit;
                }
                
            }
        }else{
            show_admin404();
        }
    }
    public function edit_vehicle()
    {
        if($_POST){
            $data = $_POST;
            $data['vehicles'] = $this->vehicle_model->get_vehicle_details($data);
            if(!empty($data['vehicles'])) {

                $htmlrespon = $this->load->view('vehicles/edit_vehicle_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['vehicles']);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function view_invoices()
    {
        if($_POST){
            $data = $_POST;
            $data['vehicles']       = $this->vehicle_model->get_vehicle_details($data);
            $data['invoices']       = $this->vehicle_model->get_invoices_details($data);
            $data['meta_count']     = count($data['invoices']);
            $sum        = 0;    $paid_sum   = 0;    $due_sum    = 0;
            foreach($data['invoices'] as $values) {
                $sum += $values[ 'total_price' ];
                $paid_sum += $values[ 'paid_price' ];
                $due_sum += $values[ 'due_price' ];
            }
            $data['total_price']            = $sum;
            $data['total_paid_price']       = $paid_sum;
            $data['total_due_price']        = $due_sum;
            if(!empty($data['vehicles'])) {
                $htmlrespon = $this->load->view('vehicles/view_invoices_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['vehicles']);
                echo json_encode($finalResult);
                exit;
            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function view_vehicle()
    {
        if($_POST){
            $data = $_POST;

            $data['vehicles']       = $this->vehicle_model->get_vehicle_details($data);
            $data['veh_id']         = $data['id'];
            $data['vehicle_meta']   = $this->vehicle_model->get_vehicle_meta_details($data);
            $data['meta_count']     = count($data['vehicle_meta']);
            if(!empty($data['vehicles'])) {
                $htmlrespon = $this->load->view('vehicles/view_vehicle_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['vehicles']);
                echo json_encode($finalResult);
                exit;
            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function update()
    {
        if($_POST){
            $data = $_POST;
            $this->form_validation->set_rules('user_id', 'User name', 'required');
            $this->form_validation->set_rules('reg_no', 'Registration number', 'required');
            $this->form_validation->set_rules('company_name', 'Company name', 'required');
            $this->form_validation->set_rules('model', 'Model', 'required');
            $this->form_validation->set_rules('year_of_make', 'Year Of Make', 'required');

            if ($this->form_validation->run($this) == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_status = $this->vehicle_model->update_vehicle($data);
                if($user_status > 0){
                    $finalResult = array('msg' => 'success', 'response'=>"Vehicles successfully updated.");
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function add_meta($id = '')
    {
        $data['veh_id']         = $id;
        $data['vehicle_meta']   = $this->vehicle_model->get_vehicle_meta_details($data);
        $data['meta_count']     = count($data['vehicle_meta']);
        $this->load->view('vehicles/add_meta',$data);
    }
    public function save_meta()
    {
        if ($_POST){
            $data = $this->input->post();
            $pro_info_id    = $this->vehicle_model->insert_vehicle_meta($data['meta'],$data['veh_id']);
             $finalResult = array('msg' => 'success', 'response' => "Vehicle Information inserted.", 'id' => $data['veh_id']);
            echo json_encode($finalResult);
            exit;
        } else {
            show_admin404();
        }
    }
}
