<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'statistics_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index() { 
        $this->load->view('statistics/statistics');
	}
    public function find_match() { 
      if ($_POST){
            $data   = $this->input->post();  
            if( !empty($data['type']) ) {
                $data['result'] = $this->statistics_model->get_result($data);
                $htmlrespon     = $this->load->view('statistics/statistics_ajax' , $data,TRUE);
                $finalResult    = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
                exit;  
            } else {
                $finalResult = array('msg' => 'error', 'response' => "Type field required");
                echo json_encode($finalResult);
                exit; 
            } 
        }else{
            show_admin404();
        }   
    }
   
}
