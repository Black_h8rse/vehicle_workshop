<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'invoice_model');
		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}
	public function index()
	{
        $data['invoices'] = $this->invoice_model->get_invoices();
        $this->load->view('invoice/invoice', $data);
	}
    public function add_invoice()
    {
        $data['users']      = get_table('users');
        if(get_session('admin_id') == '1'){
            $data['vehicles']   = get_table('vehicles');
        } else {
            $data['vehicles']   = get_data('','vehicles',array('created_by'=>get_session('admin_id')));
        } 
        $data['meta_count'] = '0';
        $this->load->view('invoice/add_invoice', $data);
    }
    public function search_reg_no()
    {
        $data = $this->input->post();
        echo "<pre>";
        print_r($data);
        die();
    }
	public function delete_invoice() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->invoice_model->delete_invoice($data['id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_invoice()
    {
        if ($_POST){
            $data = $this->input->post();
            $data['rendom_key'] = check_and_generate_key('invoices','rendom_no');
            if( $data['customer_type'] == '1' ) {
                $data['vehicle']    = get_data('','vehicles',array( 'id'=> $data['reg_id'] ));
            } 
            $invoice_id = $this->invoice_model->insert_invoice($data);
            $finalResult = array('msg' => 'success', 'response' => "Invoice successfully created.", 'id' => $invoice_id);
            echo json_encode($finalResult);
            exit;

        }else{
            show_admin404();
        }
    }
    public function edit_invoice()
    {
        if($_POST){
            $data = $_POST;
            $data['vehicles'] = $this->invoice_model->get_invoice_details($data);
            if(!empty($data['vehicles'])) {

                $htmlrespon = $this->load->view('vehicles/edit_invoice_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['vehicles']);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function view_invoice()
    {
        if($_POST){
            $data = $_POST;
            $data['vehicles']       = $this->invoice_model->get_invoice_details($data);
            $data['veh_id']         = $data['id'];
            $data['vehicle_meta']   = $this->invoice_model->get_invoice_meta_details($data);
            $data['meta_count']     = count($data['vehicle_meta']);
            if(!empty($data['vehicles'])) {

                $htmlrespon = $this->load->view('vehicles/view_invoice_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon, 'report' => $data['vehicles']);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function update()
    {
        if($_POST){
            $data = $_POST;
            $this->form_validation->set_rules('user_id', 'User name', 'required');
            $this->form_validation->set_rules('reg_no', 'Registration number', 'required');
            $this->form_validation->set_rules('company_name', 'Company name', 'required');

            if ($this->form_validation->run($this) == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_status = $this->invoice_model->update_invoice($data);
                if($user_status > 0){
                    $finalResult = array('msg' => 'success', 'response'=>"Vehicles successfully updated.");
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function add_meta($id = '')
    {
        $data['veh_id']         = $id;
        $data['vehicle_meta']   = $this->invoice_model->get_invoice_meta_details($data);
        $data['meta_count']     = count($data['vehicle_meta']);
        $this->load->view('vehicles/add_meta',$data);
    }
}
