<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends MX_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model(admin_controller().'request_model');

		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect(admin_url().'login');
		}	
	}

	public function index()
	{
        $data['requests'] = $this->request_model->get();
        $this->load->view('requests/requests', $data);
	}
	public function delete_user() {
        if ($_POST){
            $data = $this->input->post();
            $slider_id = $this->request_model->delete_user($data['user_id']);
            $finalResult = array('msg' => 'success', 'response' => "successfully Deleted.", 'id' => $data['user_id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function status_update() {
        if ($_POST){
            $data = $this->input->post();
            $this->request_model->status_update($data['id'],$data['value']);
            $finalResult = array('msg' => 'success', 'response' => "successfully updated.", 'id' => $data['id']);
            echo json_encode($finalResult);
            exit;
        }else{
            show_admin404();
        }
    }
    public function save_user()
    {
        if ($_POST){
            $data = $this->input->post();
            $this->form_validation->set_rules('f_name', 'First Name', 'required');
            $this->form_validation->set_rules('l_name', 'Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|matches[password]');

            if ($this->form_validation->run() == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $email_check = get_data('','users',array('email'=>$data['email']));
                if (empty($email_check)) {
                        $user_id = $this->request_model->insert_user($data);
                        $finalResult = array('msg' => 'success', 'response' => "User successfully inserted.", 'id' => $user_id);
                        echo json_encode($finalResult);
                        exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Email already exist');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
    public function edit_user()
    {
        if($_POST){
            $data = $_POST;
            $data['user'] = $this->request_model->get_user_details($data);
            if(!empty($data['user'])) {

                $htmlrespon = $this->load->view('users/edit_user_ajax' , $data,TRUE);
                $finalResult = array('msg' => 'success', 'response'=>$htmlrespon);
                echo json_encode($finalResult);
                exit;

            } else {
                $finalResult = array('msg' => 'error', 'response'=>"This record has been deleted from system!!!");
                echo json_encode($finalResult);
                exit;
            }
        }else{
            show_admin404();
        }
    }
    public function update()
    {
        if($_POST){
            $data = $_POST;

            $this->form_validation->set_rules('f_name', 'First Name', 'required');
            $this->form_validation->set_rules('l_name', 'Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required');

            if ($this->form_validation->run($this) == FALSE)
            {
                $finalResult = array('msg' => 'error', 'response'=>validation_errors());
                echo json_encode($finalResult);
                exit;
            }else{
                $user_status = $this->request_model->update_user($data);
                if($user_status > 0){
                    $finalResult = array('msg' => 'success', 'response'=>"User successfully updated.");
                    echo json_encode($finalResult);
                    exit;
                }else{
                    $finalResult = array('msg' => 'error', 'response'=>'Something went wrong!');
                    echo json_encode($finalResult);
                    exit;
                }
            }
        }else{
            show_admin404();
        }
    }
}
