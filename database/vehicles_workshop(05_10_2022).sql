-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 10, 2022 at 05:50 AM
-- Server version: 8.0.27
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vehicles_workshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
CREATE TABLE IF NOT EXISTS `expenses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `expense_type_id` int NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  `detail` varchar(200) NOT NULL,
  `created_by` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `expense_type_id`, `meta_key`, `meta_value`, `detail`, `created_by`, `created_at`) VALUES
(3, 3, 'Purchase Products', '123123', 'Purchase Products', 1, '2022-02-06 13:46:45.878162'),
(4, 2, 'Expense', '10000', 'asdasd asdasd', 1, '2022-02-16 13:05:55.421002');

-- --------------------------------------------------------

--
-- Table structure for table `expense_type`
--

DROP TABLE IF EXISTS `expense_type`;
CREATE TABLE IF NOT EXISTS `expense_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `expense_type`
--

INSERT INTO `expense_type` (`id`, `created_by`, `name`, `status`, `created_at`) VALUES
(1, 1, 'asdas', 1, '2022-02-06 13:25:46.647936'),
(2, 1, 'Cup&Coffee', 1, '2022-02-06 13:37:33.530207'),
(3, 1, 'Purchase Products', 1, '2022-02-06 13:45:24.436619'),
(4, 1, 'trsssdf', 1, '2022-02-16 13:06:11.652988');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `text` longtext NOT NULL,
  `status` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `created_by`, `text`, `status`, `created_at`) VALUES
(1, 5, 'this is a test message to admin', 'pending', '2022-01-03 18:56:12.912528');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `vehicle_id` int NOT NULL,
  `created_by` int NOT NULL,
  `user_id` int NOT NULL,
  `due_price` varchar(200) NOT NULL,
  `total_price` varchar(100) NOT NULL,
  `paid_price` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `rendom_no` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `type`, `vehicle_id`, `created_by`, `user_id`, `due_price`, `total_price`, `paid_price`, `created_at`, `rendom_no`) VALUES
(3, '', 5, 1, 14, '0', '300', '300', '2022-01-03 18:50:55.576149', 'PnL5c0SNUv'),
(4, '', 5, 1, 14, '555', '13555', '13000', '2022-01-16 15:55:10.538453', 'YOcaA122rs'),
(21, '', 5, 1, 14, '100', '1000', '900', '2022-02-06 10:51:41.436429', 'QIGyRxicuu'),
(24, 'spare_part', 0, 1, 0, '0', '143', '143', '2022-02-06 11:38:11.321612', 'KVQ94m5rYL'),
(23, 'vehicle', 15, 1, 4, '56', '97', '41', '2022-02-06 11:35:24.990867', 'tD2MYiLeui');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_meta`
--

DROP TABLE IF EXISTS `invoice_meta`;
CREATE TABLE IF NOT EXISTS `invoice_meta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_meta`
--

INSERT INTO `invoice_meta` (`id`, `invoice_id`, `meta_key`, `meta_value`, `price`, `date`) VALUES
(1, '3', 'service', '200', '', '2022-01-03 18:50:55.576495'),
(2, '3', 'oil change', '100', '', '2022-01-03 18:50:55.577090'),
(3, '4', 'asasad', '1231', '', '2022-01-16 15:55:10.538900'),
(4, '4', 'asdasda', '12312', '', '2022-01-16 15:55:10.563659'),
(5, '4', 'asdasds', '12', '', '2022-01-16 15:55:10.563996'),
(38, '23', 'Cumque est voluptas ', '21', '21', '2022-02-06 11:35:24.991243'),
(45, '24', '2', '1', '43', '2022-02-06 11:38:11.323991'),
(44, '24', '5', '10', '6', '2022-02-06 11:38:11.323522'),
(43, '24', '4', '1', '36', '2022-02-06 11:38:11.323039'),
(42, '24', 'address', 'Eiusmod nisi accusan', '', '2022-02-06 11:38:11.322392'),
(31, '21', 'oil change', '1000', '', '2022-02-06 10:51:41.436813'),
(41, '24', 'phone', '69', '', '2022-02-06 11:38:11.322174'),
(40, '24', 'name', 'Leilani Leach', '', '2022-02-06 11:38:11.321912'),
(39, '23', 'Esse tenetur volupt', '76', '76', '2022-02-06 11:35:24.991570'),
(46, '24', 'Sed ad illo nostrud ', 'null', '10', '2022-02-06 11:38:11.324220'),
(47, '24', 'Optio soluta molest', 'null', '48', '2022-02-06 11:38:11.324462');

-- --------------------------------------------------------

--
-- Table structure for table `material_cat`
--

DROP TABLE IF EXISTS `material_cat`;
CREATE TABLE IF NOT EXISTS `material_cat` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_cat`
--

INSERT INTO `material_cat` (`id`, `created_by`, `name`, `description`, `created_at`) VALUES
(7, 1, 'Oil', 'Use as for engine', '2021-10-22 12:31:24.353308'),
(6, 5, 'Shana George', 'Corporis sunt iure r', '2021-10-21 13:30:35.287496'),
(8, 5, 'Daryl White', 'Et commodo vitae et ', '2021-10-22 12:45:55.951758'),
(9, 23, 'Olivia Henson', 'Non sit dolorum eos123', '2021-10-22 14:09:49.336421'),
(10, 1, 'testing my code', 'testing my code', '2022-01-03 18:51:14.610701'),
(11, 1, 'asdad', 'asdasd', '2022-01-16 15:55:45.086135');

-- --------------------------------------------------------

--
-- Table structure for table `material_description`
--

DROP TABLE IF EXISTS `material_description`;
CREATE TABLE IF NOT EXISTS `material_description` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `pro_id` int NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `material_description`
--

INSERT INTO `material_description` (`id`, `created_by`, `pro_id`, `meta_key`, `meta_value`, `created_at`) VALUES
(1, 5, 4, 'Purchased', '10', '2022-01-03 19:04:45.829154'),
(2, 5, 4, 'Purchased', '10', '2022-01-03 19:04:57.482395'),
(3, 1, 3, 'sold', '200', '2022-01-16 15:56:07.067056'),
(4, 1, 3, 'Purchased', '100', '2022-01-16 15:56:19.965052'),
(5, 1, 2, 'Purchased', '856', '2022-01-16 16:27:30.399207'),
(6, 1, 5, 'Purchased', '10', '2022-02-06 08:52:31.189541'),
(7, 1, 2, 'Purchased', '20', '2022-02-06 13:44:53.203981'),
(8, 1, 5, 'Purchased', '200', '2022-02-06 13:45:58.034119'),
(9, 1, 4, 'Purchased', '197', '2022-02-06 13:46:45.866253');

-- --------------------------------------------------------

--
-- Table structure for table `material_meta`
--

DROP TABLE IF EXISTS `material_meta`;
CREATE TABLE IF NOT EXISTS `material_meta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `meterial_pro_id` int NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `material_meta`
--

INSERT INTO `material_meta` (`id`, `created_by`, `meterial_pro_id`, `meta_key`, `meta_value`, `created_at`) VALUES
(1, 1, 9, 'price_per_piece', '1001001', '2022-01-03 18:53:52.062297'),
(2, 5, 1, 'price_per_piece', '1', '2022-01-03 19:04:45.829909'),
(3, 5, 1, 'price_per_piece', '0.1', '2022-01-03 19:04:57.490649'),
(4, 1, 1, 'price_per_piece', '123123.12', '2022-01-16 15:56:19.974426'),
(5, 1, 10, 'price_per_piece', '200', '2022-01-16 15:56:42.154899'),
(6, 1, 1, 'price_per_piece', '143835.42406542055', '2022-01-16 16:27:30.400037'),
(7, 1, 1, 'price_per_piece', '200', '2022-02-06 08:52:31.201722'),
(8, 1, 1, 'price_per_piece', '624.989847715736', '2022-02-06 13:46:45.878919');

-- --------------------------------------------------------

--
-- Table structure for table `material_pro`
--

DROP TABLE IF EXISTS `material_pro`;
CREATE TABLE IF NOT EXISTS `material_pro` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `buy_price` int NOT NULL,
  `material_id` int NOT NULL,
  `sale_price` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_pro`
--

INSERT INTO `material_pro` (`id`, `created_by`, `name`, `quantity`, `buy_price`, `material_id`, `sale_price`, `created_at`) VALUES
(2, 1, 'Barclay Jensen', '24', 852, 6, 0, '2021-10-22 12:31:04.070062'),
(3, 1, 'Larissa Barnett', '71', 95, 7, 0, '2021-10-22 12:31:38.594267'),
(4, 5, 'Vielka Odom', '200', 215, 6, 0, '2021-10-22 12:31:58.802367'),
(5, 5, 'Buffy Curtis', '200', 776, 6, 0, '2021-10-22 12:45:46.391816'),
(6, 23, 'Ayanna Finley', '63', 34, 9, 0, '2021-10-22 14:10:06.895545'),
(10, 1, 'engine oil', '50', 20000, 7, 0, '2022-01-16 15:56:42.154248');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `type` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `created_by`, `type`, `status`, `created_at`) VALUES
(1, 5, 'feedback', 'success', '2022-01-03 18:56:12.913627');

-- --------------------------------------------------------

--
-- Table structure for table `request_meta`
--

DROP TABLE IF EXISTS `request_meta`;
CREATE TABLE IF NOT EXISTS `request_meta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `request_id` int NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `request_meta`
--

INSERT INTO `request_meta` (`id`, `request_id`, `meta_key`, `meta_value`, `created_at`) VALUES
(1, 1, 'text', 'this is a test message to admin', '2022-01-03 18:56:12.915470');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `created_by` int NOT NULL,
  `user_type` int NOT NULL COMMENT '1=superadmin,2=workshop_admin,3=vehicle_owners,4=spare_part_shop_owners,5=mechanic',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `email`, `password`, `phone`, `created_by`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Hassan', 'Raza', 'raza.explorelogics@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+923447621846', 0, 1, '2021-10-15 01:39:16.303218', '2021-10-15 02:15:13.213182'),
(3, 'Martha Rodriguez1', 'Eugenia Wolfe1', 'syzebunis@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+929912939123', 1, 2, '2021-10-16 15:02:30.807106', NULL),
(4, 'Martha Rodriguez1', 'Eugenia Wolfe1', 'bibimekyf@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+929912939123', 1, 2, '2021-10-16 15:07:47.950135', NULL),
(5, 'Amna', 'Arif', 'amna3454643@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+929912939123', 1, 2, '2021-10-16 15:08:32.759325', NULL),
(23, 'Shannon Kemp', 'Carson Carter', 'mechanic@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '84', 1, 4, '2021-10-22 13:45:30.324157', NULL),
(13, 'wahaaj1', 'scooby1', 'wahaj@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+913012830120', 1, 2, '2021-10-17 16:01:00.993948', '2021-10-17 16:01:13.623414'),
(14, 'Garth Cooke', 'Quyn Schultz', 'seqafuqat@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '50', 13, 2, '2021-10-17 16:13:10.943877', NULL),
(15, 'Daquan Henry', 'Joan Bray', 'kyqusocubo@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '36', 13, 2, '2021-10-17 17:05:26.273009', NULL),
(16, 'Kylan Mclean', 'MacKenzie Atkinson', 'nevexojafe@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '34', 13, 2, '2021-10-17 17:05:32.220220', NULL),
(17, 'workshop', 'workshop', 'workshop@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '1121231', 1, 2, '2021-10-21 12:47:33.358049', '2021-10-22 13:48:00.589534'),
(19, 'Bell Wynn', 'Kristen Wright', 'sanacut@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '74', 1, 2, '2021-10-21 12:51:11.234997', NULL),
(20, 'Sigourney Mendez', 'Sierra Nichols', 'duti@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '86', 1, 4, '2021-10-21 12:54:07.246597', NULL),
(21, 'Quyn Mcbride', 'Martin Richardson', 'dapikyq@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '98', 5, 3, '2021-10-21 13:21:25.567257', '2022-04-13 09:26:45.203718'),
(22, 'April Stokes', 'Ignatius Wilkerson', 'cypileny@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '51', 1, 5, '2021-10-21 13:53:00.166590', '2021-10-22 13:46:57.218107'),
(24, 'Kessie Gonzales', 'Beatrice Joyner', 'sulyto@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '88', 1, 2, '2022-01-03 18:49:00.935159', NULL),
(25, 'testing', 'testing', 'testing@mail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '12312123', 21, 3, '2022-04-13 09:36:42.772832', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `user_id` int NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `year_of_make` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `image` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `created_by`, `user_id`, `reg_no`, `model`, `year_of_make`, `company_name`, `image`, `created_at`, `updated_at`) VALUES
(5, 1, 14, 'Libero ratione ut lo', 'asdasd', '1231', 'Delaney and Dunn LLC', 0, '2021-10-17 17:06:55.369677', '2022-01-16 15:53:43.295564'),
(15, 1, 4, 'Dolorum libero aliqu', 'qwdsadas', '123123', 'Macdonald and Wiggins Inc', 0, '2021-10-17 17:50:20.499194', '2022-01-16 15:53:51.417974'),
(16, 1, 15, 'Quo beatae aut quis ', 'Lorem sunt aute prae', '1991', 'Morales Hunt Inc', 0, '2022-01-16 15:53:26.976056', '2022-01-16 15:53:26.976056'),
(17, 25, 25, '123123', '123123', '1231', '123123', 0, '2022-04-13 11:31:40.848670', '2022-04-13 11:31:40.848670');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_complains`
--

DROP TABLE IF EXISTS `vehicle_complains`;
CREATE TABLE IF NOT EXISTS `vehicle_complains` (
  `id` int NOT NULL AUTO_INCREMENT,
  `complain` varchar(200) NOT NULL,
  `complain_by` int NOT NULL,
  `complain_approve_by` int NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `vehicle_complains`
--

INSERT INTO `vehicle_complains` (`id`, `complain`, `complain_by`, `complain_approve_by`, `status`, `created_at`) VALUES
(1, 'testing', 25, 0, 2, '2022-04-13 11:32:29.402068');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_meta`
--

DROP TABLE IF EXISTS `vehicle_meta`;
CREATE TABLE IF NOT EXISTS `vehicle_meta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_by` int NOT NULL,
  `vehicle_id` int NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` varchar(100) NOT NULL,
  `rendom_key` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_meta`
--

INSERT INTO `vehicle_meta` (`id`, `created_by`, `vehicle_id`, `meta_key`, `meta_value`, `rendom_key`) VALUES
(28, 5, 14, 'asdasdasd123', '23234234asasds', 'for_future_use'),
(27, 5, 14, 'chasi number', '123', 'for_future_use'),
(26, 5, 14, 'asdasdasd123', '123123', 'for_future_use'),
(25, 5, 14, 'engine number', '123', 'for_future_use'),
(24, 5, 14, 'check check12', 'asdasdasd', 'for_future_use'),
(30, 5, 7, 'asdasdasd', 'asdasdasd12312', 'for_future_use'),
(31, 5, 7, 'asdasd', '12312', 'for_future_use'),
(32, 5, 7, 'asdasd', '123123', 'for_future_use'),
(33, 1, 5, 'engine no', 'BR26723Kl672323', 'for_future_use');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
