-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 22, 2021 at 08:08 PM
-- Server version: 5.7.21
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vehicles_workshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(100) NOT NULL,
  `created_by` int(100) NOT NULL,
  `user_id` int(110) NOT NULL,
  `due_price` varchar(200) NOT NULL,
  `total_price` varchar(100) NOT NULL,
  `paid_price` varchar(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `rendom_no` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_meta`
--

DROP TABLE IF EXISTS `invoice_meta`;
CREATE TABLE IF NOT EXISTS `invoice_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_cat`
--

DROP TABLE IF EXISTS `material_cat`;
CREATE TABLE IF NOT EXISTS `material_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_cat`
--

INSERT INTO `material_cat` (`id`, `created_by`, `name`, `description`, `created_at`) VALUES
(7, 1, 'Oil', 'Use as for engine', '2021-10-22 12:31:24.353308'),
(6, 5, 'Shana George', 'Corporis sunt iure r', '2021-10-21 13:30:35.287496'),
(8, 5, 'Daryl White', 'Et commodo vitae et ', '2021-10-22 12:45:55.951758'),
(9, 23, 'Olivia Henson', 'Non sit dolorum eos123', '2021-10-22 14:09:49.336421');

-- --------------------------------------------------------

--
-- Table structure for table `material_pro`
--

DROP TABLE IF EXISTS `material_pro`;
CREATE TABLE IF NOT EXISTS `material_pro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `buy_price` int(100) NOT NULL,
  `material_id` int(100) NOT NULL,
  `sale_price` int(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_pro`
--

INSERT INTO `material_pro` (`id`, `created_by`, `name`, `quantity`, `buy_price`, `material_id`, `sale_price`, `created_at`) VALUES
(2, 1, 'Barclay Jensen', '856', 852, 6, 0, '2021-10-22 12:31:04.070062'),
(3, 1, 'Larissa Barnett', '271', 95, 7, 0, '2021-10-22 12:31:38.594267'),
(4, 5, 'Vielka Odom', '102', 215, 6, 0, '2021-10-22 12:31:58.802367'),
(5, 5, 'Buffy Curtis', '192', 776, 6, 0, '2021-10-22 12:45:46.391816'),
(6, 23, 'Ayanna Finley', '363', 34, 9, 0, '2021-10-22 14:10:06.895545');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `created_by` int(200) NOT NULL,
  `user_type` int(10) NOT NULL COMMENT '1=superadmin,2=workshop_admin,3=vehicle_owners,4=spare_part_shop_owners,5=mechanic',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `email`, `password`, `phone`, `created_by`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Hassan', 'Raza', 'raza.explorelogics@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+923447621846', 0, 1, '2021-10-15 01:39:16.303218', '2021-10-15 02:15:13.213182'),
(3, 'Martha Rodriguez1', 'Eugenia Wolfe1', 'syzebunis@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+929912939123', 1, 2, '2021-10-16 15:02:30.807106', NULL),
(4, 'Martha Rodriguez1', 'Eugenia Wolfe1', 'bibimekyf@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+929912939123', 1, 2, '2021-10-16 15:07:47.950135', NULL),
(5, 'Amna', 'Arif', 'amna3454643@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+929912939123', 1, 2, '2021-10-16 15:08:32.759325', NULL),
(23, 'Shannon Kemp', 'Carson Carter', 'mechanic@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '84', 1, 4, '2021-10-22 13:45:30.324157', NULL),
(13, 'wahaaj1', 'scooby1', 'wahaj@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '+913012830120', 1, 2, '2021-10-17 16:01:00.993948', '2021-10-17 16:01:13.623414'),
(14, 'Garth Cooke', 'Quyn Schultz', 'seqafuqat@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '50', 13, 2, '2021-10-17 16:13:10.943877', NULL),
(15, 'Daquan Henry', 'Joan Bray', 'kyqusocubo@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '36', 13, 2, '2021-10-17 17:05:26.273009', NULL),
(16, 'Kylan Mclean', 'MacKenzie Atkinson', 'nevexojafe@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '34', 13, 2, '2021-10-17 17:05:32.220220', NULL),
(17, 'workshop', 'workshop', 'workshop@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '1121231', 1, 2, '2021-10-21 12:47:33.358049', '2021-10-22 13:48:00.589534'),
(19, 'Bell Wynn', 'Kristen Wright', 'sanacut@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '74', 1, 2, '2021-10-21 12:51:11.234997', NULL),
(20, 'Sigourney Mendez', 'Sierra Nichols', 'duti@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '86', 1, 4, '2021-10-21 12:54:07.246597', NULL),
(21, 'Quyn Mcbride', 'Martin Richardson', 'dapikyq@mailinator.com', 'b2fe8b46929bfa4c65fee9d5d43a2423799b18e360782e9abc27bd420877243e', '98', 5, 3, '2021-10-21 13:21:25.567257', NULL),
(22, 'April Stokes', 'Ignatius Wilkerson', 'cypileny@mailinator.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '51', 1, 5, '2021-10-21 13:53:00.166590', '2021-10-22 13:46:57.218107');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `image` int(100) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `created_by`, `user_id`, `reg_no`, `company_name`, `image`, `created_at`, `updated_at`) VALUES
(5, 13, 14, 'Libero ratione ut lo', 'Delaney and Dunn LLC', 0, '2021-10-17 17:06:55.369677', '2021-10-17 17:06:55.369677'),
(15, 1, 4, 'Dolorum libero aliqu', 'Macdonald and Wiggins Inc', 0, '2021-10-17 17:50:20.499194', '2021-10-17 17:50:20.499194');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_meta`
--

DROP TABLE IF EXISTS `vehicle_meta`;
CREATE TABLE IF NOT EXISTS `vehicle_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(100) NOT NULL,
  `vehicle_id` int(100) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` varchar(100) NOT NULL,
  `rendom_key` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_meta`
--

INSERT INTO `vehicle_meta` (`id`, `created_by`, `vehicle_id`, `meta_key`, `meta_value`, `rendom_key`) VALUES
(28, 5, 14, 'asdasdasd123', '23234234asasds', 'for_future_use'),
(27, 5, 14, 'chasi number', '123', 'for_future_use'),
(26, 5, 14, 'asdasdasd123', '123123', 'for_future_use'),
(25, 5, 14, 'engine number', '123', 'for_future_use'),
(24, 5, 14, 'check check12', 'asdasdasd', 'for_future_use'),
(30, 5, 7, 'asdasdasd', 'asdasdasd12312', 'for_future_use'),
(31, 5, 7, 'asdasd', '12312', 'for_future_use'),
(32, 5, 7, 'asdasd', '123123', 'for_future_use');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
